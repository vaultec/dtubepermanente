package io.permanente;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;

import io.ipfs.multiaddr.MultiAddress;

/**
 * @author vaultec
 * @since v1.0
 */
public class Config {
	/**Formatting
	 * Variables (int, float, String, Objects, JSON) 
	 * Contrustors
	 * Methods 
	 * Main (If needed)
	 * ...
	 * Add similar functionality to VideoEntry (Easy converting to storable types)
	 */
	public plugins plugins = new plugins();
	//public database database = new database();
	
	public boolean API_enabled = true;
	public boolean gc; //When enabled, expired videos will be unpinned; Experimental
	public int Worker_maxthreads = 2;
	public int API_Port = 8090;
	public int Max_Queue_Size = 16; //Maximum amount of video entries in queue at any given time.
	//public long Max_Video_Size = 2 * (10 * 9); // 2 GB, General maximum for videos (nobody needs 2GB just yet) DOES NOT WORK...
	//Switch to single Folder for config ex ~/.permanente/
	File Queue_Location; 
	File Dat_Location;
	File Vid_Location; //Very important select a directory that has plenty of space videos will be stored here.
	transient File configlocation;
	
	public String IPFS_HOST = "/ip4/127.0.0.1/tcp/5001"; // /ip4/127.0.0.1/tcp/5001 default
	public transient MultiAddress IPFS_MULTIADDR;
	//public String P2PSeed;
	
	
	public enum eplugins {
		headmaster, api, discord_bot, gatewaypusher
	}
	
	/**
	 * For all non essential modules. However, its recommended to enable at least one of these.
	 * @author vaultec
	 */
	public class plugins {
		public ArrayList<eplugins> enabled = new ArrayList<eplugins>();
		public headmaster headmaster;
		public discord_bot discord_bot;
		public gatewaypusher gatewaypusher;
		
		public class headmaster {
			public transient boolean enabled;
			public boolean dynamic_alloc; 
			public boolean require_auth; //Essentials tells whether it is public or not.
			public ArrayList<String> authorized;
			
			public long polling_delay = 30000;
			public int sat = 10;
			
			public String motd = "";
		}
		public class discord_bot {
			public transient boolean enabled;
			@Expose public boolean whitelist_enable;
			@Expose public ArrayList<Long> Whitelist = new ArrayList<Long>();
			@Expose public ArrayList<Long> ownerlist = new ArrayList<Long>();
			@Expose public String token = "";
			@Expose public String prefix = "!"; //By default set as you please
		}
		public class gatewaypusher {
			public transient boolean enabled = true;
		}
	}
	/*public class database {
		long savedelay = 15000; //Delay in ms for saving database.
		boolean compression = false;
		File savelocation;
		String encryption_key; //If key is specified database will be encrypted with key.
	}*/
	
	public class submgr {
		String account = "";
		long buffer_time = 86400 * 5; //5 Days, default.
	}
	/**
	 * always run this after initializing a new config instance or making changes.
	 */
	public void check() {
		for(eplugins e : this.plugins.enabled) {
			if(e.equals(eplugins.discord_bot) 
					&& this.plugins.discord_bot == null) {
				this.plugins.discord_bot = plugins.new discord_bot();
			}
			if(e.equals(eplugins.headmaster) 
					&& this.plugins.headmaster == null) {
				this.plugins.headmaster = plugins.new headmaster();
			}
			if(e.equals(eplugins.gatewaypusher) && this.plugins.headmaster == null) {
				this.plugins.gatewaypusher = plugins.new gatewaypusher();
			}
		}
	}
	public boolean Save() {
		if(this.configlocation == null ) {
			return false;
		}
		return Save(this.configlocation);
	}
	public boolean Save(File Location) {
		this.check();
		try (FileWriter file = new FileWriter(Location)) {
			file.write(this.ToJson());
			file.flush();
			file.close();
			return true;
		} catch (JSONException | IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	public static Config FromJson(JsonObject json) {
		return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().fromJson(json, Config.class);
	}
 	public String ToJson() {
 		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}
}