package io.permanente;

import java.io.File;
import java.util.List;
import java.util.concurrent.ConcurrentMap;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.IndexTreeList;
import org.mapdb.Serializer;
import org.mapdb.elsa.ElsaMaker;
import org.mapdb.elsa.ElsaSerializer;

import io.ipfs.multihash.Multihash;
import io.permanente.client.ClientConfig;
import io.permanente.primitives.Serializers;
import io.permanente.primitives.VideoEntry;

@SuppressWarnings("rawtypes")
public class Database {
	//private Config config;
	public DB db;
	
	public ConcurrentMap<String, VideoEntry> video_database;
	public ConcurrentMap permdb_index;
	public IndexTreeList<Multihash> raw_pins;

	public Database(Config config) {
		this(config.Dat_Location, false);
	}
	public Database(ClientConfig config) {
		this(config.pinfile, true);
	}
	private Database(File file, boolean isclient) {
		this.db = DBMaker
				.fileDB(file)
				//.checksumStoreEnable()
				.closeOnJvmShutdown()
				.checksumHeaderBypass()
				.transactionEnable()
				.make();
		if(!isclient) {//.keySerializer(Serializer.STRING).valueSerializer(new Serializers.VideoEntrySerializer())
			video_database = db.hashMap("video_database").keySerializer(Serializer.STRING).valueSerializer(new Serializers.VideoEntrySerializer()).createOrOpen(); 
			//video_database = db.hashMap("video_database").createOrOpen();
			permdb_index = db.hashMap("permdb_index").createOrOpen();
			raw_pins = db.indexTreeList("rawpins", new Serializers.MultihashSerializer()).createOrOpen();
			
		}
	}
 	public void commit() {
		this.db.commit();
	}
 	public ConcurrentMap hashmap(String name) {
 		return db.hashMap(name).createOrOpen();
 	}
 	public DB db() {
 		return this.db;
 	}
}
