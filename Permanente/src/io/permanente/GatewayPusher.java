package io.permanente;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.Instant;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.permanente.primitives.VideoEntry;

/*
 * NOTE: Experimental may use lots of ram, network or other resources 
 * use with caution, executed by default, disable if you encounter problems
 * 
 *
 */

/**
 * 
 * @author vaultec
 * @since v1.0
 */
public class GatewayPusher extends Thread {
	private Database Database;
	//Doesn't need to cache often, It cache as users watch videos through gateways, actively caching them. 
	//This may only be necessary to use gatewaypusher on new videos, as a "preloading" mechanic
	//Efficiency of gatewaypusher needs to be checked, particularly when dealing with accessing the gateways and multihreading
	//I will not be supporting multithreading for gatewaypusher due to issue with reading/modifying the Database with multiple threads
	//Need to figure out way to dynamically allocate different entries in the Database to different threads.
	//As soon as data stream starts, immediately cut connection to save time and keep processing speed fast
	//Ipfs nodes take some time to retrieve data (dependent on Internet speed), test environment Internet speed isn't very good. results may differ
	//I have choosen for a reason to push videos on more than one gateway, this allows the active gateway to have more peers to download videos, even if they might not be the best peers to donwload off of.
	
	//Time in seconds (Unix timestamp), between gateway pushes // Default; 6 hours; 
	private long Timeout = (60*60) * 6;
	private String[] Gateway_List = new String[] {
		"https://gateway.ipfs.io/ipfs/", //Probably be available for a long time
		"https://ipfs.infura.io/ipfs/"
	};
	public GatewayPusher(Database database) {
		this.Database = database;
	}
	/**
	 * Grab a URL as soon as data stream starts, cut connection (Changeable via boolean)
	 * i can't figure out which is more efficient
	 * @param URL
	 */
	private void HTTPing(String URLs, boolean CutStream) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is = null;
		try {
			is = new URL(URLs).openStream();
			byte[] byteChunk = new byte[4096];
			int n;

			while ((n = is.read(byteChunk)) > 0) {
				baos.write(byteChunk, 0, n);//Start reading data stream from IPFS gateway
				if(CutStream) {
					baos.close();
					byteChunk = null;
					return;
				}
			}
			byteChunk = null;
		} catch (IOException e) {
			// System.err.printf ("Failed while reading bytes from %s: %s",
			// url.toExternalForm(), e.getMessage());
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
					baos.close();
					is = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		int k = 0 ;
		
		for( ; ; ) {
			
			//for(Database.video_database.keys())
			//System.out.println(Database.video_database.keys().next());

			//My super simple debug print without spamming console
			if(k > 750) {
				//System.out.println(Database.video_database.toString());
				k=0;
			}
			//Some issues with nullpointer exception
			if(!Database.video_database.isEmpty()) {
				Set<String> tmp = Database.video_database.keySet(); 
				for(String e : tmp) {
					//if ( e instanceof Long ) {
						//These HTTPing(s) better not throw and error i will be pissed >:)
						if(((VideoEntry) Database.video_database.get(e)).vh_240p != null) {
							if( Instant.now().getEpochSecond() - ((VideoEntry) Database.video_database.get(e)).misc.la_240p > this.Timeout) {
								for(int x = 0; x < Gateway_List.length; x++) {
									HTTPing(Gateway_List[x] + ((VideoEntry) Database.video_database.get(e)).vh_240p.toBase58(),false);
								}
								//Database.video_database.getJSONObject((String) e).put("vh240LA", new Long(Instant.now().getEpochSecond()));
								VideoEntry work = (VideoEntry) ((VideoEntry) Database.video_database.get(e));
								work.misc.la_240p = new Long(Instant.now().getEpochSecond());
								Database.video_database.put(e, work);
							}
						}
						if(((VideoEntry) Database.video_database.get(e)).vh_480p != null) {
							if( Instant.now().getEpochSecond() - ((VideoEntry) Database.video_database.get(e)).misc.la_480p > this.Timeout) {
								for(int x = 0; x < Gateway_List.length; x++) {
									HTTPing(Gateway_List[x] + ((VideoEntry) Database.video_database.get(e)).vh_480p.toBase58(),false);
								}
								//Database.video_database.getJSONObject((String) e).put("vh480LA", new Long(Instant.now().getEpochSecond()));
								VideoEntry work = (VideoEntry) ((VideoEntry) Database.video_database.get(e));
								work.misc.la_480p = new Long(Instant.now().getEpochSecond());
								Database.video_database.put(e, work);
							}
						}
						if(((VideoEntry) Database.video_database.get(e)).vh_720p != null) {
							if( Instant.now().getEpochSecond() - ((VideoEntry) Database.video_database.get(e)).misc.la_720p > this.Timeout) {
								for(int x = 0; x < Gateway_List.length; x++) {
									HTTPing(Gateway_List[x] + ((VideoEntry) Database.video_database.get(e)).vh_720p.toBase58(),true);
								}
								//Database.video_database.getJSONObject((String) e).put("vh720LA", new Long(Instant.now().getEpochSecond()));
								VideoEntry work = (VideoEntry) ((VideoEntry) Database.video_database.get(e));
								work.misc.la_720p = new Long(Instant.now().getEpochSecond());
								Database.video_database.put(e, work);
							}
						}
						if(((VideoEntry) ((VideoEntry) Database.video_database.get(e))).vh_1080p != null) {
							if( Instant.now().getEpochSecond() - ((VideoEntry) Database.video_database.get(e)).misc.la_1080p > this.Timeout) {
								for(int x = 0; x < Gateway_List.length; x++) {
									HTTPing(Gateway_List[x] + ((VideoEntry) Database.video_database.get(e)).vh_1080p,true);
								}
								VideoEntry work = (VideoEntry) ((VideoEntry) Database.video_database.get(e));
								work.misc.la_1080p = new Long(Instant.now().getEpochSecond());
								Database.video_database.put(e, work);
							}
						}
					}
				//}
			}
			
			
			try {
				TimeUnit.MICROSECONDS.sleep(10); //10 ms instruction loop delay
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			k=k+1;
			//return;
		}
	}
}
