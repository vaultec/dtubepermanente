package io.permanente;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import io.ipfs.api.IPFS;
import io.ipfs.api.MerkleNode;
import io.ipfs.cid.Cid;
import io.ipfs.multihash.Multihash;
import io.permanente.Config.eplugins;
import io.permanente.api.JettyServer;
import io.permanente.plugins.Bot;
import io.permanente.primitives.DObject;
import io.permanente.primitives.IPFSVideo;
import io.permanente.primitives.Session;
import io.permanente.primitives.VideoEntry;


/*
 * Java References
 * <https://github.com/stleary/JSON-java>
 * <https://github.com/marvin-we/steem-java-api-wrapper>
 * <https://github.com/ipfs/java-ipfs-api>
 * <https://github.com/Hive2Hive/Hive2Hive/wiki> Untilize for P2P
 */


/**
 * Main class for entire permanente instance.
 * 
 * @author vaultec
 * @since v1.0
 * @version v0.2
 */
public class Permanente implements Runnable {
	/**
	 * General project todo/notes
	 * NOTE: Ensure design to be sufficient for large tear drop grid storage servers (AKA pools)
	 */
	
	public Config config = new Config();
	public final IPFS ipfs;
	

	public Database Database;
	public HashMap<String, Session> HTTPsessions = new HashMap<String, Session>();//String is the cookie of the session as the key
	public ArrayList<VideoEntry> List_Queue = new ArrayList<VideoEntry>();
	public Worker[] ActiveWorkers = new Worker[this.config.Worker_maxthreads];
	
	public final dtube dtube = new dtube();
	
	/**
	 * Test contructor, designed for static configs. Non configurable.
	 * @deprecated
	 */
	public Permanente() {
		//By Default
		/**
		 * TODO
		 * Add Config loading/saving
		 */
		//this.config.put("Queue_Cache", new File(System.getProperty("user.dir") +"/Queue.json"));
		
		//TODO: Remember to remove these config, if set in config.json 
		System.out.println("No config specified! Data will be stored in current working directory");
		this.config.Queue_Location = new File(System.getProperty("user.dir") +"/queue.json");
		this.config.Vid_Location = new File(System.getProperty("user.dir") +"/Videos/");
		this.config.Dat_Location = new File(System.getProperty("user.dir") +"/dat.json");
		try {
			this.ipfs = new IPFS("/ip4/127.0.0.1/tcp/5001");
		} catch (Exception ex) {
			System.out.println("[ERROR] IPFS daemon is not avaliable.");
			throw new java.lang.RuntimeException("IPFS daemon is not avaliable.");
		}
		
		
		try {
			Scanner in = new Scanner(new FileReader(this.config.Dat_Location));
			StringBuilder sb = new StringBuilder();
			while(in.hasNext()) {
			    sb.append(in.next());
			}
			in.close();
			Type type = new TypeToken<HashMap<String, VideoEntry>>(){}.getType();
			Gson g = new Gson();
			this.Database = g.fromJson(sb.toString(),type);
		} catch (Exception e) {
			System.out.println("[INFO] failed to read database, Probably doesnt exist new one will be created");
		}
		
		//I have to add this to fix issue with database being over written, if it actually fixes it 
		Runtime.getRuntime().addShutdownHook(new Thread() {
	        public void run() {
	            try {
	                Thread.sleep(200);
	                System.out.println("shutting down ...");
	                //some cleaning up code...
	                //TODO create proper cleanup code
	                //SaveData();
	            } catch (InterruptedException e) {
	                e.printStackTrace();
	            }
	        }
	    });
	}
	/**
	 * Setups a permanente instance with specified config. If config is missing required elements. It will fill in with defaults.
	 * @param config
	 */
	public Permanente(Config config) {
		//Super basic config checker
		this.config = config;
		if(config.Vid_Location == null) {
			this.config.Vid_Location = new File(System.getProperty("user.dir") +"/videos/");
		} else {
			this.config.Vid_Location = config.Vid_Location;
		}
		if(config.Queue_Location == null) {
			this.config.Queue_Location = new File(System.getProperty("user.dir") +"/queue.json");
		} else {
			this.config.Queue_Location = config.Queue_Location;
		}
		if(config.Dat_Location == null) {
			this.config.Dat_Location = new File(System.getProperty("user.dir") +"/datmap.db");
		} else {
			this.config.Dat_Location = config.Dat_Location;
		}
		if(!config.IPFS_HOST.equals("") && config.IPFS_HOST != null) {
			this.ipfs = new IPFS(config.IPFS_HOST);
		} else {
			throw new java.lang.RuntimeException("[Error] IPFS node not specified");
		}
		
		if(config.configlocation == null) {
			this.config.configlocation = new File(System.getProperty("user.dir") +"/config.json");
		} else {
			this.config.configlocation = config.configlocation;
		}
		
		
		/*
		 * @deprecated
		try {
			Scanner in = new Scanner(new FileReader(this.config.Dat_Location));
			StringBuilder sb = new StringBuilder();
			while(in.hasNext()) {
			    sb.append(in.next());
			}
			in.close();
			Type type = new TypeToken<HashMap<String, VideoEntry>>(){}.getType();
			Gson g = new Gson();
			this.Database = g.fromJson(sb.toString(),type);
		} catch (Exception e) {
			System.out.println("[INFO] failed to read database, Probably doesnt exist new one will be created");
		}*/ 
		
		this.Database = new Database(config);
		try {
			//this.Database.start();
		} catch(Exception ex) {
			ex.printStackTrace();
			System.err.println("[ERROR] Failed to start db");
		}
		/*try {
			try {
				//this.p2p = new P2P();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (NoSuchAlgorithmException e2) {
			e2.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
			System.err.println("[Error] Failed to start P2P engine.");
		}*/
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
	        public void run() {
	            try {
	                Thread.sleep(200);
	                System.out.println("shutting down ...");
	                //some cleaning up code...
	                //TODO create proper cleanup code
	                //SaveData();
	            } catch (InterruptedException e) {
	                e.printStackTrace();
	            }
	        }
	    });
	}

	
	
	/**
	 * Managements all dtube functionalities. 
	 * 
	 * @author vaultec
	 */
	public class dtube {
		/**
		 * Returns video metadata for specified URL 
		 * @param String URL
		 * @return JSONObject Metadata
		 */
		public JSONObject GetVideoMetadata(String URL) {
				URL = URL.replace("https://d.tube/#!/v/", "");
				String[] param = {
						"",
						""
				};
				
				
				//Simple hack for steemit/busy/dtube
				String[] tmp = URL.split("/");
				param[0] = tmp[tmp.length-2].replace("@", "");
				param[1] = tmp[tmp.length-1];
				
			   return GetVideoMetadata(param);
		}
		/**
		 * Returns video metadata for specified param
		 * Param should be [0] Author [1] Permlink
		 * TODO: Add support for smoke.
		 * @param String[] Param
		 * @return JSONObject metadata
		 */
		public JSONObject GetVideoMetadata(String[] Param) {
			String[] Steemit_APIs = {
					"https://api.steemit.com/",
					//"https://api.steemitdev.com/",
					//"https://steemd.privex.io/",
					"https://steemd.minnowsupportproject.org/"
			};
			
			JSONObject json = new JSONObject();
			json.put("id", 0);
			json.put("jsonrpc", "2.0");
			json.put("method", "get_content");
			JSONArray jr = new JSONArray();
			jr.put(Param[0]);
			jr.put(Param[1]);
			json.put("params", jr);
			HttpClient httpClient = new DefaultHttpClient(); 
			String response1 = new String();
			try {
				//Get all the data from API
				HttpPost request = new HttpPost(Steemit_APIs[new Random().nextInt(Steemit_APIs.length)]); //Messy i know
				StringEntity params = new StringEntity(json.toString());
				request.addHeader("content-type", "application/json");
				request.setEntity(params);
				HttpResponse response = httpClient.execute(request);

				//handle response here...
				HttpEntity responseEntity = response.getEntity();
				if(responseEntity!=null) {
					response1 = EntityUtils.toString(responseEntity);
				}

			}catch (Exception ex) {
				//handle exception here
				ex.printStackTrace();
				throw new java.lang.RuntimeException("Error has occurred during HTTP JSON request");
			} finally {
				//Dont do anything
			}
			try {
				return new JSONObject(response1);
			} catch (JSONException ex) {
				System.out.println("Failed to convert HTTP response to JSON Object");
				//throw new java.lang.RuntimeException("Failed to convert HTTP Response into JSON Object");
			} 
			return null;
		}
		
		/**
		 * Creates entry from URL
		 * @param URL
		 * @return
		 */
		public VideoEntry CreateEntry(String URL) {
			JSONObject Meta = GetVideoMetadata(URL);
			VideoEntry Entry = new VideoEntry();
			System.out.println(Meta);
			JSONObject json_metadata = new JSONObject((String) Meta.getJSONObject("result").get("json_metadata"));
			JSONObject ab = json_metadata.getJSONObject("video").getJSONObject("content");
			
			Entry.video_metadata = json_metadata;
			if(ab.has("video240hash"))
				Entry.vh_240p = Multihash.fromBase58(ab.getString("video240hash"));
			if(ab.has("video480hash"))
				Entry.vh_480p = Multihash.fromBase58(ab.getString("video480hash"));
			Entry.misc.author = Meta.getJSONObject("result").getString("author");
			Entry.misc.permlink = Meta.getJSONObject("result").getString("root_permlink");
			if(ab.has("video720hash"))
				Entry.vh_720p =  Multihash.fromBase58(ab.getString("video720hash"));
			if(ab.has("video1080hash"))
				Entry.vh_1080p =  Multihash.fromBase58(ab.getString("video1080hash"));

			return Entry;
		}
		
		public void PinVideo(IPFSVideo video) {
			//ipfs.pin.add(video.hash());
		}
		
		/**
		 * Returns VideoEntry of request ID
		 * @param long key
		 * @return
		 */
		public VideoEntry videoinfo(long key) {
			for(Object e : Database.video_database.keySet()) {
				//System.out.println(this.config.Database.get(e).ID);
				if(((VideoEntry) Database.video_database.get(e)).ID == key) {
					return (VideoEntry) Database.video_database.get(e);
				}
			}
			return null;
		}
		
		public ArrayList<Multihash> hashList(VideoEntry e) {
			ArrayList<Multihash> list = new ArrayList<Multihash>();
			if(e.vh_240p != null) {
				list.add(e.vh_240p);
			}
			if(e.vh_480p != null) {
				list.add(e.vh_480p);
			}
			if(e.vh_720p != null) {
				list.add(e.vh_720p);
			}
			if(e.vh_1080p != null) {
				list.add(e.vh_1080p);
			}
			return list;
		}
		
		public ArrayList<Multihash> hashList(List<VideoEntry> inList) {
			ArrayList<Multihash> list = new ArrayList<Multihash>();
			for(VideoEntry e : inList) {
				for(Multihash m :  hashList(e)) {
					list.add(m);
				}
			}
			return list;
		}
		
		public ArrayList<Multihash> FullIndex() {
			Iterator<String> it = Database.video_database.keySet().iterator();
			ArrayList<Multihash> fullist = new ArrayList<Multihash>();
			while(it.hasNext()) {
				Object key = it.next();
				VideoEntry e = (VideoEntry) Database.video_database.get(key);
				ArrayList<Multihash> list = hashList(e);
				fullist.addAll(list);
			}
			return fullist;
		}

		/**
		 * Add video to Queue from supplied argument (VideoEntry)
		 * Returns int, success code, if error returns error 
		 * TODO: Create universal error list, to sort out confusion between different methods, better error handling
		 * @param VideoEntry Entry
		 * @return int
		 */
		public int AddVideoToQueue(VideoEntry Entry) {
			
			
			//State Codes 
			int Vid_Exists = -1;
			int In_Queue = -2;
			int Queue_full = -3;
			int In_worker = -4;
			
			if(List_Queue.size() >= config.Max_Queue_Size) {
				return Queue_full;
				//throw new java.lang.RuntimeException("Erorr: Video queue has reached maximum size limit of " + this.config.Max_Queue_Size);
			}
			
			/*
			for(Object o: this.Queue){
			    if ( o instanceof JSONObject ) {
			    	if(((JSONObject)o).get("URL").equals(URL)) {
			    		return In_Queue;
				    	//throw new java.lang.RuntimeException("Erorr Video is already in Queue");
			    	}
			    }
			}*/
			
			for(VideoEntry o : List_Queue) {
				if(o.ID == Entry.ID) {
					System.out.println("[INFO] Video already in queue");
					return In_Queue;
				}
			}
			
			if(Database.video_database.containsValue(Entry)) {
				System.out.println("[INFO]: Video Already exists, Video ID : " + Entry.ID);
				return Vid_Exists;
			}
			
			//Deprecated modify to use something else
			for(Worker e : ActiveWorkers) {
				//if(e.GetEntry().getString("URL").equals(URL)) {
				if(e != null) {
					if(e.GetEntry().ID == Entry.ID) {
						System.out.println("[INFO] Video already in worker");
						return In_worker;
					}
				}
			}
			//Search table
			for(Object e : Database.video_database.keySet()) {
				if(((VideoEntry) Database.video_database.get(e)).misc.permlink.equals(Entry.misc.permlink)) {
					System.out.println("[INFO] Video with same permlink already exists of ID " + ((VideoEntry) Database.video_database.get(e)).ID);
					return Vid_Exists;
				}
			}
			
			
			List_Queue.add(Entry);
			return 0;
		
			/*
			//JSONObject Entry = new JSONObject();
			
			//By default all videos should contain these values 
			Entry.put("URL",URL);
			Entry.put("videohash", ab.get("videohash"));
			Entry.put("vh240", ab.get("video240hash"));
			Entry.put("vh480", ab.get("video480hash"));
			Entry.put("author", Meta.getJSONObject("result").getString("author"));
			Entry.put("permlink", Meta.getJSONObject("result").getString("root_permlink"));
			
			
			//Check if video contains 720p most times not 
			if(ab.has("video720hash"))
				Entry.put("vh720", ab.get("video720hash"));
			if(ab.has("video1080hash"))
				Entry.put("vh1080", ab.get("video1080hash"));
			
			
			
			this.Queue.put(Entry);
			
			//HTTPing("https://cloudflare-ipfs.com/ipfs/"+ ab.get("video480hash"));
			return 0;*/
		}
		
		/**
		 * Removes video from IPFS and database
		 * Requires ID of video, ID can be obtained through searching database for specific URL
		 * @param long ID
		 * @return
		 */
		public int RemoveVideo(long ID) {
			int success = 0;
			int video_not_found = -1;
			int Other_Error = -2;
			//URL = URL.replace("https://d.tube/#!/v/", "");
			//String key = URL.split("/")[0] + "@" + URL.split("/")[1];
			VideoEntry Entry = null;
			
			String id = Long.toString(ID);
			if(Database.video_database.containsKey(id)) {
				//JSONObject Entry = this.config.Database.getJSONObject(key);
				Entry = (VideoEntry) Database.video_database.get(id);
				if(Entry.vh_240p != null) {
					Multihash filePointer = Entry.vh_240p;
					try {
						ipfs.pin.rm(filePointer);
					} catch (IOException e) {
						e.printStackTrace();
						return Other_Error;
					}
				}
				if(Entry.vh_480p != null) {
					Multihash filePointer = Entry.vh_480p;
					try {
						ipfs.pin.rm(filePointer);
					} catch (IOException e) {
						e.printStackTrace();
						return Other_Error;
					}
				}
				if(Entry.vh_720p != null) {
					Multihash filePointer = Entry.vh_720p;
					try {
						ipfs.pin.rm(filePointer);
					} catch (IOException e) {
						e.printStackTrace();
						return Other_Error;
					}
				}
				if(Entry.vh_1080p != null) {
					Multihash filePointer = Entry.vh_1080p;
					try {
						ipfs.pin.rm(filePointer);
					} catch (IOException e) {
						e.printStackTrace();
						return Other_Error;
					}
				}
			} else {
				System.out.println("[INFO] Non existent video was attempting to be removed");
				return video_not_found;
			}
			//this.config.Database.remove(URL.split("/")[0] + "@" + URL.split("/")[1]);
			Database.video_database.remove(id);
			return success;
		}
		
		/**
		 * Pins video to IPFS.
		 * @param Entry
		 * @throws IOException 
		 */
		public void PinVideo(VideoEntry Entry) throws IOException {
			try {
				if(Entry.vh_240p != null) {
					ipfs.pin.add(Entry.vh_240p);
				}
				if(Entry.vh_480p != null) {
					ipfs.pin.add(Entry.vh_480p);
				}
				if(Entry.vh_720p != null) {
					ipfs.pin.add(Entry.vh_720p);
				}
				if(Entry.vh_1080p != null) {
					ipfs.pin.add(Entry.vh_1080p);
				}
			} catch(IOException ex) {
				System.err.println("[Error] Failed to pin video");
				throw ex;
			}
		}
		
		public void UnpinVideo(VideoEntry Entry) {
			if(Database.video_database.containsValue(Entry)) {
				//JSONObject Entry = this.config.Database.getJSONObject(key);
				if(Entry.vh_240p != null) {
					Multihash filePointer = Entry.vh_240p;
					try {
						ipfs.pin.rm(filePointer);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if(Entry.vh_480p != null) {
					Multihash filePointer = Entry.vh_480p;
					try {
						ipfs.pin.rm(filePointer);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if(Entry.vh_720p != null) {
					Multihash filePointer = Entry.vh_720p;
					try {
						ipfs.pin.rm(filePointer);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if(Entry.vh_1080p != null) {
					Multihash filePointer = Entry.vh_1080p;
					try {
						ipfs.pin.rm(filePointer);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} else {
				System.out.println("[INFO] Non existent video was attempting to be removed");
			}
		}
	}
	
	public int getsize(Multihash hash) {
		try {
			MerkleNode node = ipfs.object.get(hash);
			int size = 0;
			for(MerkleNode e : node.links) {
				size+= e.size.get();
			}
			return size;
		} catch (IOException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public boolean SaveData() {
		
		//Save Queue
		//TODO Convert queue to JSON respectively (With Gson)
		try {
			Files.write(new Gson().toJson(this.List_Queue).getBytes(), this.config.Queue_Location);
			//System.out.println("Written Queue");
		} catch (JSONException | IOException e) {
			//TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Save Database
		//try {
			//Files.write(new Gson().toJson(this.Database).getBytes(), this.config.Dat_Location);
			//System.out.println("Written Database");
		//} catach (JSONException | IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		//}
		this.Database.commit();
		return true;
	}
	private void gc() {
		Iterator<String> keys = this.Database.video_database.keySet().iterator();
		while(keys.hasNext()) {
			Object key = keys.next();
			if(((VideoEntry) this.Database.video_database.get(key)).expire != 0) {
				if(((VideoEntry) this.Database.video_database.get(key)).expire > System.currentTimeMillis()) {
					this.dtube.UnpinVideo(this.Database.video_database.get(key));
					System.out.println("[INFO] Video: " + key +" has expired/removed!");
				}
			}
		}
	}
	public void shutdown() {
		try {
			this.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	/**
	 * Main Action Thread
	 */
	@Override
	public void run() {
		
		//System.out.println(new Gson().toJson(IPFSVideo.fetchFromCid(Cid.decode("zdpuApiJvMNKctMSzLiRShoUkT2jsn8LVXJSC3hNaiAGwNRbT"), ipfs)));
		getsize(Cid.fromBase58("QmPq5RbYT6LuX54Qj4R5RTXDkch2ymdmjaHotfaoz7DMjM"));
		if(Database.video_database == null) {
			System.err.println("[Error] Database is null");
			System.exit(-1);
		}
		/*try {
			System.out.println(new Gson().toJson(ipfs.dht.findprovs(Multihash.fromBase58("QmS8TGZdscG3oh7tPJ4bCzEX91VPgLDB5ABqFDTTuJ3pLV"))));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("Finished test");*/
		
		
		GatewayPusher gp = new GatewayPusher(this.Database);
		if(config.plugins.enabled.contains(eplugins.gatewaypusher)) {
			gp.start();
		}
		if(this.config.API_enabled) {
			JettyServer js = new JettyServer(this);
			try {
				js.start();
				System.out.println("Started API on port: " + this.config.API_Port);
			} catch (Exception e) {
				e.printStackTrace();
				System.out.print("[ERROR] Failed to start API Server");
			}
		}
		
		int tim = 0;
		int tim2 = 0;
		int tim3 = 0;
		/**
		 * Action Loop
		 */
		for( ; ; ) {
			
			//Queue Checker, A little messed up needs revision
			if(!List_Queue.isEmpty()) {
				//Goes through list of active threads and checks 
				for(int x = 0; x < ActiveWorkers.length; x++ ) {
					if(ActiveWorkers[x] != null) {
						if(!ActiveWorkers[x].isAlive()) {
							//System.out.println(this.Queue.get(0));
							//ActiveWorkers[x] = new Worker(this.Queue.getJSONObject(0), this.config, this.ipfs);
							ActiveWorkers[x] = new Worker(this.List_Queue.get(0), this.config,this.Database, this.ipfs, this);
							ActiveWorkers[x].start();
							//Queue.remove(0);
							this.List_Queue.remove(0);
							break;
						} else {
							//check code here
							//System.out.println("Uncatted123 " + Queue.toString());
							//Block is triggered when thread is full
						}
					} else {
						//Initialize new Thread Aka Worker
						//ActiveWorkers[x] = new Worker(this.Queue.getJSONObject(0), this.config, this.ipfs);
						ActiveWorkers[x] = new Worker(this.List_Queue.get(0), this.config,this.Database, this.ipfs, this);
						ActiveWorkers[x].start();
						this.List_Queue.remove(0);
						break;
					}
				}
			}
			
			
			if(tim >=2500) {
				try {
					
					/*JSONObject json = new JSONObject();
					json.put("v", "permanente_v0.2");
					ipfs.pubsub.pub("foo", json.toString());
					Stream<Map<String, Object>> out = ipfs.pubsub.sub("foo");
					Iterator<Map<String, Object>> it = out.iterator();
					while (true) {
						if(it.hasNext()) {
							System.out.println(new String(Base64.getDecoder().decode(it.next().get("data").toString().getBytes())));
						}
					}*/
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				};
				//System.out.println(new Gson().toJson(this.config.Database));
				SaveData();
				tim=0;
			}
			if(this.config.gc) {
				if(tim2 >= 300000) {
					gc();
					tim2=0;
				}
			}
			if(tim3 > 300 * 1000) {
				int activet = 0;
				for(Thread e : this.ActiveWorkers) {
					if(e != null) {
						if(e.isAlive()) {
							activet++;
						}
					}
				}
				System.out.println("[Stats] Active workers: " + activet 
				+ " Queue size: " 
				+ this.List_Queue.size() + " Active slave nodes " + this.HTTPsessions.size());
				tim3=0;
			}
			
			
			try {
				TimeUnit.MICROSECONDS.sleep(1); //1 ms instruction loop delay
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			tim=tim+1;
			tim2=tim2+1;
			tim3=tim3+1;
		}
	}
	
	public static void main(String[] args) {
		Bot bot  = null;
		Options options = new Options();
		options.addOption("d","default",false,"Use default configuration location. (%Workingdirectory%/Config.json) RECOMMENDED");
		options.addOption("s","saveconfig",false, "Saves configuration that is specified.");
		options.addOption("st","exitonsave",false,"Exits program after data save.");
		options.addOption("cf","configfile",true,"Specify config file location.");
		options.addOption("hm","headmaster",false,"Operate a headmaster on API. (Enables API) RUNS BY DEFAULT IF API IS ENABLED");
		options.addOption("h","help", false, "Prints help menu");
		options.addOption("a","api",false,"When flag is set, API server is started");
		options.addOption("ap","apiport",true,"Set API port, 0-65563");
		options.addOption("fh","ipfshost",true,"Specify ipfs host, must be in ipfs host format. (defaults to localhost:5001)");
		
		
		options.addOption("bt","bot.token",true,"Specify discord bot token. (defaults to config without) TODO");
		options.addOption("bot","bot",false,"Starts discord bot. TODO");
		//options.addOption("r","run",false,"Runs client");
		
		HelpFormatter formatter = new HelpFormatter();		
		if(args.length == 0) {
			formatter.printHelp("Permanente v0.2", options);
			return;
		}
		
		CommandLine cmd = null;
		try {
			cmd = new DefaultParser().parse(options, args);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		//Startup, checks for config file locations
		File ConfigFile = null;
		
		if(cmd.hasOption("d")) {
			ConfigFile = new File(System.getProperty("user.dir") +"/config.json"); //Default value
		} else if(cmd.hasOption("cf")) {
			try {
				ConfigFile = new File(cmd.getOptionValue("cf")); //Parse give argument into file
			} catch (Exception ex) {
				System.out.println("[ERROR] Invalid file path " +cmd.getOptionValue("cf"));
				return;
			}
		} else {
			System.out.println("[Error] Config file not specified please use --default or --configfile <config file location> to specify config.");
			return;
		}
		
		
		
		//Command line parsing order
		//Read config file
		//Modify/Override config in memory
		//Option to save
		//Then start working with config
		//System.out.println(new Gson().toJson(new IPFSVideo()));
		
		Scanner in = null;
		try {
			in = new Scanner(new FileReader(ConfigFile));
			in.useDelimiter(System.getProperty("line.separator"));
		} catch (FileNotFoundException e) {
			System.out.println("[Error] Config not found, Creating new one.");
			Config conf = new Config();
			byte[] array = new byte[32]; new Random().nextBytes(array);
			//conf.P2PSeed = Base64.getEncoder().withoutPadding().encodeToString(array);
			conf.Save(ConfigFile);
			return;
		}
		//byte[] array = new byte[32]; new Random().nextBytes(array);
		//System.out.println(Base64.getEncoder().withoutPadding().encodeToString(array));
		StringBuilder sb = new StringBuilder();
		while(in.hasNext()) {
		    sb.append(in.next());
		}
		in.close();
		
		
		Config config = new Gson().fromJson(sb.toString(),Config.class);
		config.check();
		config.Save(ConfigFile);
		
		if(cmd.hasOption("hm")) {
			config.API_enabled = true;
			config.plugins.headmaster.enabled = true;
			if(!config.plugins.enabled.contains(Config.eplugins.discord_bot)) {
				config.plugins.enabled.add(Config.eplugins.discord_bot);
			}
		}
		if(cmd.hasOption("h")) {
			formatter.printHelp("Permanente v0.2 (warning help menu outdated)", options);
			return;
		}
		if(cmd.hasOption("ap")) {
			try {
				config.API_Port = Integer.parseInt(cmd.getOptionValue("ap"));
			} catch (NumberFormatException ex) {
				System.out.print("[Error] Invalid port number: "+cmd.getOptionValue("ap"));
				return;
			}
		}
		if(cmd.hasOption("fh")) {
			config.IPFS_HOST = cmd.getOptionValue("fh");
		}
		if(cmd.hasOption("bt")) {
			config.plugins.discord_bot.token = cmd.getOptionValue("bt");
		}
		if(cmd.hasOption("bot")) {
			config.plugins.discord_bot.enabled = true;
		}
		if(cmd.hasOption("s")) {
			config.Save(ConfigFile);
			System.out.println("Saved config!");
			if(cmd.hasOption("st")) {
				System.out.println("Program shutting down on save.");
				return;
			}
		}
		
		Permanente Program = new Permanente(config);
		
		Thread RunningThread = new Thread(Program);
		RunningThread.start();
		
		
		//Work in progress
		if(config.plugins.enabled.contains(eplugins.discord_bot)) {
			if(config.plugins.discord_bot.token == null | config.plugins.discord_bot.token == "") {
				System.err.println("[Error] Bot token not specified. Bot will not be started");
				System.exit(-1);
			}
			bot = new Bot(Program);
			bot.run();
		}
	}
}