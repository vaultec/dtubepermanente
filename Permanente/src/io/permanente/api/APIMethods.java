package io.permanente.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import io.ipfs.api.MerkleNode;
import io.ipfs.multihash.Multihash;
import io.permanente.Permanente;
import io.permanente.Config.plugins.headmaster;
import io.permanente.api.Clustermananger.AuthenticationException;
import io.permanente.api.Clustermananger.InternalException;
import io.permanente.api.Clustermananger.authrequest;
import io.permanente.primitives.Session;
import io.permanente.primitives.VideoEntry;

/**
 * TODO: remove json.org dependency, use wrapper (class) for interacting with
 * clients, errors, etc.. Serialization for wrapper. TODO: Create method to
 * easily manage requests, treat this class like a background thread contains a
 * bunch of different variables. TODO: Debug output. Simple switch for cluster networks. Separate API and cluster network
 * features. API is stateless simple request and response. Cluster network is
 * dynamic management.
 * 
 * API has two goals, one being provide stateless API for adding videos. The 2nd is for cluster networks.
 * May decide on having sessions for stateless (But logging does just as good of a job)
 * 
 * TODO: Ensure DAPI can function behind cloudflare and/or apache2 (Check HTTP headers for actual client IP)
 * TODO: Change stateless API calls to use both GET and POST requests 
 * I want API be a combination of GET/POST methods.. GET for web interface and things that dont need much input..
 * POST for more complicated inputs like JSON, DAPI, etc.
 * 
 * CLARIFICATION: DAPI stands for dynamic API. It is meant for server sessions active server/client interaction.
 * 
 * TODO: Add filter method that will redirect data to the requested API method. Similar to DAPI. Include CORS headers.
 * 
 * @author vaultec
 * @since v0.2
 * @version v0.2.0 beta
 */
@Path("v1")
public class APIMethods extends Thread {
	Permanente permanente;
	private Clustermananger clusternetwork;

	public APIMethods(Permanente perm) {
		this.permanente = perm;
		clusternetwork = new Clustermananger(this.permanente);
		this.clusternetwork.start(); //Management thread is REQUIRED.. Use different system for telling clients DAPI is not enabled..
		
		//if (this.permanente.config.headmaster.enabled) {
		//	this.clusternetwork.start();
		//}
	}

	@POST
	@Consumes({ "application/json", "application/x-www-form-urlencoded" })
	@Produces({ "application/json", "application/x-www-form-urlencoded" })
	@Path("add_videoFromURL")
	public String add_videoFromURL(String in) {
		JSONObject response = new JSONObject();
		System.out.println("Attempting to add video");
		// response.addProperty("Test", "AAAAAAAAAA=");

		JsonElement jelement = new JsonParser().parse(in);
		JsonObject jobject = jelement.getAsJsonObject();

		// response.put("id", jobject.get("id").getAsInt());

		System.out.print(jobject.get("URL").getAsString());
		VideoEntry ve;
		if (jobject.has("URL")) {
			ve = this.permanente.dtube.CreateEntry(jobject.get("URL").getAsString());
		} else {
			response.put("error", -10);
			response.put("message", "Invalid query, missing element URL");
			return response.toString();
		}

		System.out.print(new Gson().toJson(ve));
		System.out.print("Creating video with ID of " + ve.ID);

		int status;
		try {
			status = this.permanente.dtube.AddVideoToQueue(ve);
		} catch (Exception ex) {
			return ex.getMessage();
		}

		if (status == -1) {
			response.put("error", -1);
			response.put("message", "video already exists in database");
		}
		if (status == -2) {
			response.put("error", -2);
			response.put("message", "video already exists in queue.");
		}
		if (status == -3) {
			response.put("error", -3);
			response.put("message", "video already exists in queue.");
		}
		if (status == 0) {
			response.put("vid_id", ve.ID);
		}
		// System.out.println(jobject);

		return response.toString();
	}

	/**
	 * Add video to queue, with specifying a videoEntry. Returns status code,
	 * other metadata. 
	 * 
	 * @param in
	 * @return
	 */
	@POST
	@Consumes({ "application/json", "application/x-www-form-urlencoded" })
	@Produces({ "application/json", "application/x-www-form-urlencoded" })
	@Path("add_video")
	public String add_video(String in) {
		//TODO: Parse video info... Convert to VideoEntry then add.
		return null;
	}

	@POST
	@Consumes({ "application/json", "application/x-www-form-urlencoded" })
	@Produces("application/json")
	@Path("remove_video")
	public String remove_video(String in) {
		JSONObject response = new JSONObject();

		return response.toString();
	}
	
	@GET
	@Produces("application/json")
	@Path("getvideo")
	public String getvideo(@QueryParam("id") long id) {
		System.out.println(id);
		if(this.permanente.Database.video_database.get(Long.toString(id)) == null) {
			return "Video does not exist!";
		}
		return new Gson().toJson(this.permanente.Database.video_database.get(Long.toString(id)), VideoEntry.class);
	}
	
	/**
	 * CLUSTER NETWORK STUFF BELOW KEEP, STATELESS METHODS ^ABOVE^ THIS MARKER.
	 * KEEP THINGS ORGANIZED HERE!
	 */

	/**
	 * Gives config information about server put whatever verbose information
	 * here. Included for future usage TODO: Build good format for it work
	 * 
	 * @param in
	 * @return
	 */
	@POST
	@Consumes({ "application/json", "application/x-www-form-urlencoded" })
	@Produces({ "application/json", "application/x-www-form-urlencoded" })
	@Path("config")
	public String config(String in) {
		JSONObject response = new JSONObject();
		JsonObject jobject = new JsonParser().parse(in).getAsJsonObject();
		JSONObject config = new JSONObject();

		config.put("auth_required", this.permanente.config.plugins.headmaster.require_auth);
		config.put("dynamic_alloc", this.permanente.config.plugins.headmaster.dynamic_alloc);

		response.put("config", config);
		return response.toString();
	}
	
	@POST
	@Consumes({ "application/json", "application/x-www-form-urlencoded", "text/plain;charset=UTF-8" })
	@Produces({ "application/json", "application/x-www-form-urlencoded" })
	@Path("rawvideoadd")
	public String rawadd(String in) {
		JSONObject response = new JSONObject();
		JsonObject jobject = new JsonParser().parse(in).getAsJsonObject();
		
		VideoEntry video = new VideoEntry();
		JsonObject content = jobject.getAsJsonObject("video").getAsJsonObject("content");
		
		try {
			if(content.has("video240hash")) 
				video.vh_240p = Multihash.fromBase58(content.get("video240hash").getAsString());
			if(content.has("video480hash")) 
				video.vh_480p = Multihash.fromBase58(content.get("video480hash").getAsString());
			if(content.has("video720hash")) 
				video.vh_720p = Multihash.fromBase58(content.get("video720hash").getAsString());
			if(content.has("video1080hash")) 
				video.vh_1080p = Multihash.fromBase58(content.get("video1080hash").getAsString());
		} catch(Exception ex) {
			response.put("error", error.ParseError());
			ex.printStackTrace();
			return response.toString();
		}
		try {
			this.permanente.dtube.PinVideo(video);
			this.permanente.Database.video_database.put(Long.toString(video.ID), video);
		} catch (IOException e) {
			response.put("error", error.InternalError());
			e.printStackTrace();
			return response.toString();
		}
		//System.out.println(x);
		/*
		 * TODO: Write here code for serializing CIDs from String NOT Object. 
		 * Include other information such as signing before it gets sent to steemit API
		 */
		response.put("status", "success");
		
		
		return response.toString();
	}
	
	@GET
	@Produces({ "application/json", "application/x-www-form-urlencoded" })
	@Path("headmaster_publish/get")
	public String publish() {
		return this.clusternetwork.headmaster.get_index().toString();
	}
	
	@POST
	@Consumes({ "application/json", "application/x-www-form-urlencoded" })
	@Produces({ "application/json", "application/x-www-form-urlencoded" })
	@Path("headmaster_publish")
	public String publish(String in) {
		JSONObject response = new JSONObject();
		JsonObject jobject = new JsonParser().parse(in).getAsJsonObject();

		if (jobject.has("id")) {
			response.put("id", jobject.get("id").getAsInt());
		} else {
			response.put("error", -5);
			response.put("message", "invalid query, does not contain ID ");
			return response.toString();
		}
		try {
			//response.put("index", this.permanente.Database.video_database.keySet());
			Iterator<String> it = this.permanente.Database.video_database.keySet().iterator();
			ArrayList<Multihash> fullist = new ArrayList<Multihash>();
			while(it.hasNext()) {
				it.next();
				VideoEntry e = (VideoEntry) this.permanente.Database.video_database.get(it);
				ArrayList<Multihash> list = this.permanente.dtube.hashList(e);
				fullist.addAll(list);
			}
			response.put("index", fullist);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			//this.permanente.HTTPsessions.get(jobject.get("cookie").getAsString());
		} catch(Exception e) {
			e.printStackTrace();
		}
		// System.out.println(response);
		if(jobject.has("cookie")) {
			Session session = this.permanente.HTTPsessions.get(jobject.get("cookie").getAsString());
			session.updateping();
			this.permanente.HTTPsessions.put(session.cookie, session);
		}
		return response.toString();
	}
	@POST
	@Consumes({ "application/json", "application/x-www-form-urlencoded" })
	@Produces({ "application/json", "application/x-www-form-urlencoded" })
	@Path("headmaster_getvideo")
	public String getvideo(String in) {
		JSONObject response = new JSONObject();
		JsonObject jobject = new JsonParser().parse(in).getAsJsonObject();

		if (jobject.has("id")) {
			response.put("id", jobject.get("id").getAsInt());
		} else {
			response.put("error", -5);
			response.put("message", "invalid query, does not contain ID ");
			return response.toString();
		}
		if (jobject.has("entryid")) {
			try {
				VideoEntry ev = (VideoEntry) this.permanente.Database.video_database.get(jobject.get("entryid").getAsString());
				response.put("result", new Gson().toJson(ev, VideoEntry.class));
				//System.out.println(response);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		} else {
			response.put("error", "-6");
			response.put("message", "Invalid query, does not contain entry ID");
			return response.toString();
		}

		return response.toString();
	}
	/**
	 * TODO: Switch over to subscribe instead of authenticate. I will not
	 * require authentication in order to use dynamic allocation. Just
	 * recommended to have trust for the people who are using dynamic
	 * allocation. Need to have hardening protocol for dynamic allocation
	 * (Efficient way to make sure every node in a network has the piece of
	 * videos it has been allocated)
	 * 
	 * Has sub methods for subscription process.
	 * Acts like JSON-RPC. I like RPC design for state based protocol.
	 * 
	 * TODO: Change name to something more universal (Designed for RPC like request)
	 * 
	 * @param in
	 * @return
	 */
	@POST
	@Consumes({ "application/json", "application/x-www-form-urlencoded" })
	@Produces({ "application/json", "application/x-www-form-urlencoded" })
	@Path("dapi")
	public String dapi(String in, @Context HttpHeaders httpheader, @Context HttpServletResponse httpresponse) {
		try {
			JSONObject response = new JSONObject(); //Define our response
			JsonObject jobject = new JsonParser().parse(in).getAsJsonObject(); /*Parse JSON data, Need try catch block for errors.*/response.put("jsonrpc", "2.0");/*Basic header*/ 
			
			//BASIC HANDLING
			//Requires ID, return error if not... 
			if (jobject.has("id")) {
				response.put("id", jobject.get("id").getAsInt());
			} else {
				response.put("error", error.InvalidRequest());
				return response.toString();
			}
			
			//Vars
			String method = null;
			String cookie = null;
			JsonObject params = null;
			
			//Require method and params	
			if(jobject.has("method") && jobject.has("params")) {
				method = jobject.get("method").getAsString();
				params = jobject.get("params").getAsJsonObject();
			} else {
				response.put("error", error.InvalidRequest());
				return response.toString();
			}
			
			//Check for cookie.. Not a big deal if it does not exist (For now)
			//If a method requires authentication it will throw an error to client.
			
			if(!method.equals("subscribe")) {
				if(jobject.has("cookie")) {
					Session session = this.permanente.HTTPsessions.get(jobject.get("cookie").getAsString()); //Get session
					if(session == null) {
						response.put("error", error.NotAuthorized());
						return response.toString();
					}
					session.updateping(); //Update last ping of the client
					this.permanente.HTTPsessions.put(session.cookie, session); //Push that to memory (never stored on database)
					cookie = jobject.get("cookie").getAsString();
					if(!this.clusternetwork.isvalid(cookie)) {
						response.put("error", error.NotAuthorized());
						return response.toString();
					}
				} else {
					response.put("error", error.NotAuthorized());
					return response.toString();
				}
			}
			
			
			response.put("method", method);
			JSONObject result = null;
			switch(method) {
				case "ping":
					try {
						result =  this.clusternetwork.ping(params,cookie);
					} catch(InternalException e) {
						response.put("error", error.InternalError());
						e.printStackTrace();return response.toString();
					}
					response.put("result", result);
					break;
				case "subscribe":			
					Session retsession;
					try {
						retsession = this.clusternetwork.createsession(new Gson().fromJson(params, authrequest.class));
					} catch (JsonSyntaxException | AuthenticationException e) {
						response.put("error", error.InternalError());
						e.printStackTrace();return response.toString();
					}
					result = new JSONObject();
					result.put("session", new Gson().toJson(retsession,Session.class));
					result.put("motd",this.permanente.config.plugins.headmaster.motd);
					response.put("result", result);
					break;
				default:
					response.put("error", error.MethodNotFound());
					break;
			}
			return response.toString();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * Put different error codes here. I didnt want to steal with writing custom exceptions just yet.
	 * Defined server codes..: 
	 * -32001 : Invalid session (session does not exist)
	 * -32002 : Not authorized (authenticated session is required)
	 * 
	 * @author vaultec
	 */
	static class error {
		static JSONObject MethodNotFound() {
			JSONObject error = new JSONObject();
			error.put("error", -32601);
			error.put("message", "Method not found");
			return error;
		}
		static JSONObject InternalError() {
			JSONObject error = new JSONObject();
			error.put("error", -32603);
			error.put("message", "Internal error");
			return error;
		}
		static JSONObject InvalidRequest() {
			JSONObject error = new JSONObject();
			error.put("code", -32600);
			error.put("message", "Invalid request. Required arguments not supplied!");
			return error;
		}
		static JSONObject SessionExpire() {
			JSONObject error = new JSONObject();
			error.put("code", -32001);
			error.put("message", "Invalid or expired session");
			return error;
		}
		static JSONObject ParseError() {
			JSONObject error = new JSONObject();
			error.put("code", -32700);
			error.put("message", "Parse error");
			return error;
		}
		static JSONObject NotAuthorized() {
			JSONObject error = new JSONObject();
			error.put("code", -32002);
			error.put("message", "Not authorized");
			return error;
		}
	}
}