package io.permanente.api;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import io.permanente.Permanente;

/*
 * use this instead of the garbage other solutions
 * <https://github.com/briandilley/jsonrpc4j/blob/master/src/test/java/com/googlecode/jsonrpc4j/util/JettyServer.java>
 */
public class JettyServer  {
    private Server server;
    private Permanente permanente;
    
    public JettyServer(Permanente perm) {
    	this.permanente = perm;
    }
    
    public void start() throws Exception {
    	
    	Server server = new Server(this.permanente.config.API_Port);

        ServletContextHandler ctx = 
                new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
        
        ctx.setContextPath("/");
        server.setHandler(ctx);
        ctx.addServlet(new ServletHolder(new ServletContainer(resourceConfig())), "/api/*");
        server.start();
    }
    public void stop() throws Exception {
    	if(server.isRunning()) {
    		server.stop();
    	}
    }
    private ResourceConfig resourceConfig() {
        return new ResourceConfig().register(new APIMethods(this.permanente));
    }
}