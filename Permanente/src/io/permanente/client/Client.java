package io.permanente.client;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.json.JSONException;
import org.json.JSONObject;
import org.mapdb.HTreeMap.KeySet;

import com.google.gson.Gson;

import io.ipfs.api.IPFS;
import io.ipfs.multihash.Multihash;
import io.permanente.Database;
import io.permanente.Util;
import io.permanente.client.ClientInterface.AuthenticationException;
import io.permanente.exceptions.ConnectionException;
import io.permanente.primitives.Serializers;
import io.permanente.primitives.VideoEntry;

/**
 * WORK IN PROGRESS DO NOT USE THIS CLASS UNTIL VERSION v0.3 ATLEAST FOR PRODUCATION ENVIRONMENTS
 * This defines what a client looks like for teardrop style grid storage network.
 * These should be relatively basic but employ cryptography to keep Client to Server connections secure.
 * A few other functions should be added but not necessary. Like telling headmaster what to pin.
 * This should *only* function as a storage node. NOT the full functionality of the Permanente/Server.
 * Must employ usage of command line arguments for .NET Wrapper. Should be done for server too for easy setup.
 * Logging is a must keep logs to specified directory if not /tmp/ or %TEMP%
 * 
 * For now there is ZERO dynamic allocation.
 * 
 * TODO: Move to seperate project/export so i dont have to pack all the needed stuff for Permanente. Keep lightweight.
 * TODO: Add error handling/support for connection time outs without crash... Connection recovery.
 * TODO: Create interface to handle everything (in progress)
 * TODO: CLEAN UP CODE REMOVE REDUNDANT ELEMENTS.
 * TODO: Proper debug logging... Future plans for all classes/methods
 * 
 * This whole project is to support the content creator
 * @author vaultec
 * @since v0.2
 */
public class Client extends Thread {
	
	public ClientConfig config;
	public Database Database;
	private KeySet<Multihash> video_index;
	
	protected ArrayList<Multihash> queue = new ArrayList<Multihash>();
	public IPFS ipfs;

	private ClientWorker dlthread; 
	private ClientInterface ClientInterface;
	private boolean connected = false;
	
	
	
	public Client() {
		//Please dont call this contructor this not the proper way to use this...
	}
	public Client(ClientConfig Config) {
		this.config = Config;
		if(config.pinfile == null) {
			this.config.pinfile = new File(System.getProperty("user.dir") +"/pinfile.json");
		} else {
			this.config.pinfile = config.pinfile;
		}
		
		
		try {
			this.ipfs = new IPFS(this.config.IPFS_HOST);
		} catch(Exception ex) {
			System.out.println("[Error] Invalid IPFS host.");
			return;
		}
		this.ClientInterface = new ClientInterface();
		this.ClientInterface.host = this.config.Headmaster;
		this.ClientInterface.setuser(this.config.client_id);
		
		/*
		 * @deprecated
		 * try {
			Scanner in = new Scanner(new FileReader(this.config.pinfile));
			StringBuilder sb = new StringBuilder();
			while(in.hasNext()) {
			    sb.append(in.next());
			}
			in.close();
			Type type = new TypeToken<HashMap<String, VideoEntry>>(){}.getType();
			Gson g = new Gson();
			this.Database = g.fromJson(sb.toString(),type);
		} catch (Exception e) {
			System.out.println("[INFO] failed to read database, Probably doesnt exist new one will be created");
		}*/
		this.Database = new Database(config);
		video_index = Database.db()
				.hashSet("index")
				.serializer(new Serializers.MultihashSerializer())
				.createOrOpen();
		
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
	        public void run() {
	            try {
	                Thread.sleep(200);
	                System.out.println("shutting down ...");
	                //some cleaning up code...
	                //TODO create proper cleanup code
	                //SaveData();
	            } catch (InterruptedException e) {
	                e.printStackTrace();
	            }
	        }
	    });
	}
	public HashMap<Long, Multihash> distance_set(List<Multihash> list) {
		HashMap<Long, Multihash> out = new HashMap<Long, Multihash>();
		
		Multihash node_id = null;
		try {
			node_id = Multihash.fromBase58((String)ipfs.id().get("ID"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		for(Multihash e : list) {
			int dist = Util.datahelper.difference(node_id, e);
			out.put(new Long(dist), e);
		}
		System.out.println(out);
		return out;
	}
	private JSONObject get_index() {
		JSONObject js = null;
		try {
			js = this.ClientInterface.getpublish();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (ConnectionException e) {
			e.printStackTrace();
			return null;
		}
		if(js == null) {
			return null;
		}
		//Check if response contains the index file.
		if(js.has("index")) {
			//Go through list of keys in index
			
			
			
			/*for(Object pins : ) {
				if(!this.video_index.containsKey(key)) {
					VideoEntry Entry = this.ClientInterface.getvideo(key);
					if(!this.queue.contains(Entry)) {
						this.queue.add(Entry);
						System.out.println("[INFO] Add new video to queue: " +Entry.ID);
					}
				}
			}*/
			
		} else {
			System.out.println(js);
			System.out.println("[WARNING] Client failed to parse reponse from headmaster ");
		}
		return js;
	}
	private TreeMap<Long, Multihash> submap(HashMap<Long, Multihash> map, int end) {
		TreeMap<Long, Multihash> out = new TreeMap<Long, Multihash>();
		Iterator<Long> keys = map.keySet().iterator();
		int num = 0;
		for(int x = 0; end > x; x++) {
			if(keys.hasNext()) {
				Long key = keys.next();
				out.put(key, map.get(key));
			}
		}
		return out;
	}
	@Override
	public void run() {
		
		try {
			this.ClientInterface.authenticate();
		} catch (AuthenticationException e1) {
			System.out.println("[ERROR] Failed to establish session with headmaster");
			e1.printStackTrace();
		}
		
		int x = 0;
		for( ; ; ) {
			if(x > this.config.advanced_config.API_timeout) {
				//Get the json, IPNS can replace this potentially
				JSONObject js = this.get_index();
				
				System.out.println(js); //Debug
				
				//Get inital index as object (string)
				List<Object> index = js.getJSONArray("index").toList();
				
				//Get sat. Ratio for how many nodes per object.
				int sat = js.getInt("sat");
				
				//Get average 
				float avg_n = js.getFloat("avg_n");
				
				
				if(avg_n == 0) //Safety check
					avg_n = 1;
				//Calculate initial depth. AKA the ratio needed to maintain sat. 
				//Round up because there is no fractions of an object.
				int depth = (int) Math.ceil(sat / avg_n);
				
				//Convert the incoming index from base58 to decoded multihash object.
				List<Multihash> list = new ArrayList<Multihash>();
				for(Object e : index) {
					list.add(Multihash.fromBase58(e.toString()));
				}
				
				//Generate distanced tree of hashes. distance (long), hash (multihash).
				TreeMap<Long, Multihash> distance_list = new TreeMap<Long, Multihash>(this.distance_set(list));
				
				for(Entry<Long, Multihash> e : distance_list.entrySet()) { //Debug
					System.out.println("distance: " + e.getKey() + " hash: " + e.getValue().toBase58()); //Debug
				}
				
				System.out.println(depth + " " + sat + " " + avg_n);
				
				System.out.println(distance_list);
				
				//Cap size of depth. Total depth into index of hashes to store.  
				if(depth > list.size()) {
					depth = list.size();
				} else {
					depth = list.size();
				}
				
				ArrayList<Multihash> pinlist = new ArrayList<Multihash>(); //Predefine list of pins to add into worker.
				ArrayList<Multihash> unpinlist = new ArrayList<Multihash>(); //Predefine list of pins to remove.
				
				//A little concerned with future efficiency of this.
				//Calculate pin list, if the local index does not contain hash from headmaster, pin.
				for(Multihash e : list) {
					if(this.video_index.contains(e) && !this.queue.contains(e)) {
						pinlist.add(e);
					}
				}
				
				//Calculate unpin list, if the headmaster index does not contain hash in local index; unpin.
				for(Multihash e : this.video_index) {
					if(list.contains(e)) {
						unpinlist.add(e);
					}
				}
				for(Multihash e : pinlist) {
					if(!this.queue.contains(e)) 
						this.queue.add(e);
				}
				
				//Move to own threat
				for(Multihash e : unpinlist) {
					try {
						ipfs.pin.rm(e);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				
				
				System.out.println(depth + " " + sat + " " + avg_n);

				x=0;
			}
			if(this.queue.size() != 0) {
				if (this.dlthread != null) {
					if (!this.dlthread.isAlive()) {
						//this.dlthread = new ClientWorker(this.queue.get(0),this, this.video_index, this.ipfs);
						this.dlthread.start();
					}
				} else {
					//this.dlthread = new ClientWorker(this.queue.get(0),this, this.video_index, this.ipfs);
					this.dlthread.start();
				}
			}
			
			
			try {
				TimeUnit.MILLISECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			x=x+1;
		}
	}
	public boolean SaveData() {
		this.Database.commit();
		return true;
	}
	public static void main(String[] args) {
		Options options = new Options();
		options.addOption("d","default",false,"Use default configuration location. (%Workingdirectory%/clientconfig.json) RECOMMENDED");
		options.addOption("s","saveconfig",false, "Saves configuration that is specified.");
		options.addOption("st","exitonsave",false,"Exits program after data save.");
		options.addOption("cf","configfile",true,"Specify config file location.");
		options.addOption("hm","headmaster",true,"Specify headmaster URL. (Overrides config)");
		options.addOption("h","help",false,"Displays with help menu.");
		options.addOption("u","userid",true,"Specify userid. (Overrides config)");
		options.addOption("t","token",true,"Specify authentication token.(Overrides config)");
		//options.addOption("r","run",false,"Runs client");
		
		HelpFormatter formatter = new HelpFormatter();		
		if(args.length == 0) {
			formatter.printHelp("Permanente Client v0.2", options);
			return;
		}
		
		CommandLine cmd = null;
		try {
			cmd = new DefaultParser().parse(options, args);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		//Startup, checks for config file locations
		File ConfigFile = null;
		if(cmd.hasOption("d")) {
			ConfigFile = new File(System.getProperty("user.dir") +"/clientconfig.json"); //Default value
		} else if(cmd.hasOption("cf")) {
			try {
				ConfigFile = new File(cmd.getOptionValue("cf")); //Parse give argument into file
			} catch (Exception ex) {
				System.out.println("[ERROR] Invalid file path " +cmd.getOptionValue("cf"));
				return;
			}
		} else {
			System.out.println("[Error] Config file not specified please use --default or --configfile <config file location> to specify config.");
			return;
		}
		
		Scanner in = null;
		try {
			in = new Scanner(new FileReader(ConfigFile));
		} catch (FileNotFoundException e) {
			System.out.println("[Error] Config not found, Creating new one.");
			new ClientConfig().Save(ConfigFile);
			return;
		}
		StringBuilder sb = new StringBuilder();
		while(in.hasNext()) {
		    sb.append(in.next());
		}
		in.close();
		ClientConfig config = new Gson().fromJson(sb.toString(),ClientConfig.class);
		if(cmd.hasOption("u")) {
			config.client_id = cmd.getOptionValue("u");
		}
		if(cmd.hasOption("t")) {
			config.authentication_token = cmd.getOptionValue("t");
		}
		if(cmd.hasOption("hm")) {
			config.Headmaster = cmd.getOptionValue("hm");
		}
		if(cmd.hasOption("h")) {
			formatter.printHelp("Permanente Client v0.2", options);
			return;
		}
		
		if(cmd.hasOption("s")) {
			config.Save(ConfigFile);
			System.out.println("Saved config!");
			if(cmd.hasOption("st")) {
				System.out.println("Program shutting down on save.");
				return;
			}
		}
		System.out.println("[INFO] Starting client. v0.2");
		Client client = new Client(config);
		client.start();
	}
}
