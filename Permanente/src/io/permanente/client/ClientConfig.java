package io.permanente.client;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.json.JSONException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import io.permanente.Config;

/**
 * 
 * @author vaultec
 * @since v0.2
 */
public class ClientConfig {
	public IPFS_CONFIG IPFSCONF = new IPFS_CONFIG();
	public Advanced_config advanced_config = new Advanced_config();
	
	public long max_usage = 50 * (1000000 * 1000); //Maximum storage usage in bytes
	
	public String[] APIs = {}; //A list of APIs for steemit
	public String[] Smoke_APIs = {}; //A list of APIs for smoke.
	
	public String authentication_token = ""; //Token used for authentication to headmaster, if needed
	public String IPFS_HOST = "/ip4/127.0.0.1/tcp/5001"; //IPFS address, must be in IPFS address format 
	public String client_id = ""; //Unique ID used for authentication
	public String Headmaster = "https://api.dtube.network/"; //URL to headmaster 
	
	public File pinfile; //File location to database
	
	public class Advanced_config {
		public int API_timeout = 500; //Time in ms to delay connection to API shouldn't be more like 45K or less than 100
	}
	
	/**
	 * IPFS specific configuration goes here.
	 *
	 */
	public class IPFS_CONFIG {
		
	}
	public boolean Save(File Location) {
		try (FileWriter file = new FileWriter(Location)) {
			file.write(this.ToJson());
			file.flush();
			file.close();
			return true;
		} catch (JSONException | IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	public Config FromJson(JsonObject json) {
		return new Gson().fromJson(json, Config.class);
	}
 	public String ToJson() {
 		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}
}
