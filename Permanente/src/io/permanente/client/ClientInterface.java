package io.permanente.client;

import java.io.IOException;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiConsole;
import org.glassfish.jersey.internal.util.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import static org.fusesource.jansi.Ansi.Color.*;

import com.google.gson.Gson;

import io.permanente.primitives.VideoEntry;
import io.ipfs.api.IPFS;
import io.permanente.exceptions.ConnectionException;
import io.permanente.exceptions.ConnectionException.ConnectionState;
import io.permanente.primitives.JsonException;
import io.permanente.primitives.Session;

/**
 * Put all API method(s) here
 * Creates and maintains connection with headmaster
 * 
 * TODO: Error handling 
 * TODO: Authentication, PROPER TRANSPORT PROTOCOL.
 * 
 * TODO: support for DAPI disabling cluster network capabilities (Minor)
 * TODO: Proper contructor 
 * 
 * 
 * @author vaultec
 * @since v0.2
 * @version v0.2.0 beta
 */
public class ClientInterface extends Thread {
	String cookie;
	String host;
	Session session = new Session();
	
	private String token;
	private String userid;
	private boolean auth_required;
	private int timeout = 5000; //Default timeout 5000 milliseconds.
	
	interface methods {
		public final String getvideo = "/api/v1/headmaster_getvideo";
		public final String publish = "/api/v1/headmaster_publish";
		public final String index = "/api/v1/headmaster_index";
		
		public final String dapi = "/api/v1/dapi"; //TODO
		public final String config = "/api/v1/config";
	}
	
	
	public ClientInterface() {
		//JSONObject js = this.RESTQuery(this.method_config,new JSONObject("{}"));
		//this.auth_required = js.getBoolean("auth_required");
	}
	
	//TODO ACTUAL RESTQUERY
	
	/**
	 * 
	 * @param path for request
	 * @param query JSONObject
	 * @return
	 * @throws ConnectionException 
	 */
	protected JSONObject DAPIQuery(String path, JSONObject query) throws ConnectionException {
		//TODO: CREATE SINGLE METHOD FOR JSON REST API OR ALTERNATIVELY USE A CLASS/ADAPTER FOR INTERACTION TO SERVER
		query.put("cookie", this.session.cookie);	
		HttpClient httpClient = new DefaultHttpClient(); 
		String response1 = new String();
		JSONObject js = new JSONObject();
		try {
			//Get all the data from API
			HttpPost Httprequest = new HttpPost(this.host + path); //Messy i know
			StringEntity params = new StringEntity(query.toString());
			Httprequest.addHeader("content-type", "application/json");
			Httprequest.setEntity(params);
			HttpResponse response = httpClient.execute(Httprequest);

			//handle response here...
			HttpEntity responseEntity = response.getEntity();
			if(responseEntity!=null) {
				response1 = EntityUtils.toString(responseEntity);
				
			}

		} catch (IllegalStateException | ParseException ex ) {
			//handle exception here
			//Misc exceptions here. things unimportant
			ex.printStackTrace();
			//throw new java.lang.RuntimeException("Error has occurred during HTTP JSON request");
		} catch(IOException ex) {
			System.err.println("[Error] Failed to connect to headmaster.");
			//return null;
			ex.printStackTrace();
			throw new ConnectionException(ex.getMessage(), ConnectionState.ConnectionRefuse);
		}
		
		
		try {
			js = new JSONObject(response1);
		} catch(JSONException ex) {
			ex.printStackTrace();
			System.out.println(response1);
			System.out.println("[Error] failed to parse JSON output");
			throw new ConnectionException(ex.getMessage(), ConnectionState.InvalidResponse);
			//throw new JSONException(ex);
		}
		
		return js;
	}
	/**
	 * Executes raw request!
	 * @param URL
	 * @param Method (GET or POST)
	 * @return
	 * @throws ConnectionException 
	 */
	private HttpResponse rawrequest(String URL, String Method, String param) throws ConnectionException {
		HttpClient httpClient = new DefaultHttpClient();
		//String response1 = new String();
		try {
			//Get all the data from API
			HttpResponse response = null;
			if(Method.equals("POST") && param != null) {
				HttpPost Httprequest = new HttpPost(URL); //Messy i know
				StringEntity params = new StringEntity(param.toString());
				Httprequest.setEntity(params);
				Httprequest.addHeader("content-type", "application/json");
				response = httpClient.execute(Httprequest);

			} else {
				HttpGet Httprequest = new HttpGet(URL); //Messy i know
				response = httpClient.execute(Httprequest);
			}
			return response;
			//response1 = EntityUtils.toString(responseEntity);
		} catch (IllegalStateException | ParseException ex ) {
			//handle exception here
			//Misc exceptions here. things unimportant
			ex.printStackTrace();
			//throw new java.lang.RuntimeException("Error has occurred during HTTP JSON request");
		} catch(IOException ex) {
			System.err.println("[Error] Failed to connect to headmaster.");
			//return null;
			throw new ConnectionException(ex.getMessage(), ConnectionState.ConnectionRefuse);
		}
		return null; //WIP
	}
	protected HttpResponse rawrequest(String URL, String Method) throws ConnectionException {
		return rawrequest(URL, Method, null);
	}
	/**
	 * Returns packaged json request with all necessary elements.
	 * DAPI ONLY/POST
	 * DO NOT PUT IN COOKIE HERE DONT BY DAPI QUERY
	 * @param params
	 * @param method
	 * @return
	 */
	private JSONObject RPCWrap(JSONObject params, String method) {
		JSONObject output = new JSONObject();
		output.put("id", 0);
		output.put("jsonrpc", "2.0");
		output.put("params", params);
		output.put("method", method);
		
		return output;
	}
	/**
	 * Returns VideoEntry by querying headmaster/API
	 * @param ID
	 * @return
	 */
	public VideoEntry getvideo(Object ID) {
		JSONObject query = new JSONObject();
		
		query.put("id", 0);
		query.put("entryid", ID);
		
		JSONObject js = null;
		try {
			js = this.DAPIQuery(methods.getvideo, query);
		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(js.has("result")) {
			return new Gson().fromJson(js.getString("result"), VideoEntry.class);
		} else {
			System.out.println("[ERROR] Invalid result from headmaster.");
			return null;
			//Throw error code here
		}
	}
	public JSONObject getpublish() throws ConnectionException, JSONException {
		/*new JSONObject().put("id", 0).put("jsonrpc","2.0").put("params", new JSONObject());
		JSONObject in = this.RPCWrap(new JSONObject(), "ping");
		JSONObject out = null;
		try {
			out = this.DAPIQuery(this.method_dapi, in);
		} catch (ConnectionException e) {
			e.printStackTrace();
		}
		if(out == null) {
			return null;
		}
		out = out.getJSONObject("result");
		if(out.has("error")) {
			int code = out.getJSONObject("error").getInt("code");
			String msg = out.getJSONObject("error").getString("message");
			if(code == -32002) {
				throw new JsonException.AuthenticationException(msg);
				//throw new JsonException.AuthenticationException(msg);
			}
		}
		return out;*/
		HttpResponse reponse = rawrequest(this.host + "/api/v1/headmaster_publish/get", "GET");
		
		try {
			return new JSONObject(EntityUtils.toString(reponse.getEntity()));
		} catch (JSONException e) {
			throw e;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}	
	}
	public void settoken(String in) {
		this.token = in;
	}
	public void setuser(String in) {
		this.userid = in;
	}
	public void setTimeout(int in) {
		this.timeout = in;
	}
	/**
	 * TODO: replace with throwing authentication exception
	 * @return boolean authentication success.
	 * @throws AuthenticationException 
	 */
	public boolean authenticate() throws AuthenticationException {
		//TODO: Authenticate with server, using secure system. Server generates salt, sends to client.. 
		//User hashes their token with the salt. Server expects a certain response since it has the clients token.
		//Salt is essentially a long 128 Bit cookie. All requests are time stamped, and *should* contain a incremental value to project again replay attacks.
		//This is designed to be secure enough to handle any future modules (financial stuff). Though not sure yet.
		//Alternative solution would be hashing token with a salt then sending it to server. Server generates random cookie which is sent in every request.
		//Designed for usage under HTTPS conditions 
		
		JSONObject js = new JSONObject();
		js.put("id", 0);
		js.put("method", "subscribe");
		JSONObject auth = new JSONObject();
		auth.put("userid", this.userid);
		auth.put("token", this.token);
		auth.put("timestamp", Instant.now().toEpochMilli());
		
		js.put("params", auth);
		
		JSONObject reponse = null;
		try {
			reponse = this.DAPIQuery(methods.dapi, js);
		} catch (ConnectionException e) {
			e.printStackTrace();
		}
		
		if(reponse.has("error")) {
			//throw new AuthenticationException(reponse.getJSONObject("error").getString("message"));
			//throw new JsonException.
		}
		//System.out.println(reponse.toString());
		this.session = new Gson().fromJson(reponse.getJSONObject("result").getString("session"), Session.class);
		
		AnsiConsole.systemInstall();
		
		Ansi  Ansi = new Ansi();
		System.out.println(Ansi.fg(GREEN).a("[MOTD] " + reponse.getJSONObject("result").getString("motd")));
		
		return false;
	}
	class AuthenticationException extends Exception
	{
	    public AuthenticationException(String s) 
	    { 
	        // Call constructor of parent Exception 
	        super(s); 
	    } 
	} 
	public void run() {
		for( ; ; ) {
			//Ping with latest data set;
			
			
			try {
				TimeUnit.MILLISECONDS.sleep(this.timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
