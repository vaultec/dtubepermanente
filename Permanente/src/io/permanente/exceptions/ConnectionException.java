package io.permanente.exceptions;

public class ConnectionException extends Exception {
	private static final long serialVersionUID = 2159858356236206528L;
	
	public enum ConnectionState {
		ConnectionRefuse, InvalidResponse;
	}
	private ConnectionState state;
	
	
	public ConnectionException(String message, ConnectionState state) {
		super(message);
		this.state = state;
	}
	public ConnectionException(String message, Throwable cause, ConnectionState status) {
        super(message, cause);
	}
}
