package io.permanente.plugins;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Scanner;

import javax.security.auth.login.LoginException;

import com.google.gson.Gson;

import io.permanente.Config;
import io.permanente.Permanente;
import io.permanente.client.ClientConfig;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

//A little bit of code i stole from one of my older projects... originally a bot for monitoring a mining pool :P
//First *real* example of how to use permanente externally
public class Bot extends ListenerAdapter  {
	static String prefix = "!";
	Permanente Permanente; //Insert a permanente instance here either from external class or start one locally
	
	public Bot(Permanente perm) {
		this.Permanente = perm;
	}
	/**
	 * Designed for external usage not internal 
	 */
	public void run() {
		if(this.Permanente.config.plugins.discord_bot.token == null) {
			Scanner reader = new Scanner(System.in);
			System.out.println("Discord token is not present in config. Enter it here: "); //I dont want to store tokens in source for now.
			this.Permanente.config.plugins.discord_bot.token = reader.next();
			reader.close();
		}
		
		JDA jda = null;
		try {
			jda = new JDABuilder(AccountType.BOT).setToken(this.Permanente.config.plugins.discord_bot.token).addEventListener(this).build();
			System.out.println("Finished Building JDA!");
			jda.awaitReady();
		} catch (LoginException e) {
			System.err.println("Error login token invalid or failed to login.");
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		jda.getPresence().setGame(Game.playing("Backing up peoples videos"));
	}
	/**
	 * @deprecated create new instance of bot instead of calling main
	 * For reference only until v0.3
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		//Remove main class, only use constructor for easy extendability 
		//@DEPRECATED
		
		File ConfigFile = new File(System.getProperty("user.dir") +"/Config.json");
		
		Scanner in = null;
		try {
			in = new Scanner(new FileReader(ConfigFile));
		} catch (FileNotFoundException e) {
			System.out.print("[Error] Config not found, Creating new one.");
			new ClientConfig().Save(ConfigFile);
			return;
		}
		StringBuilder sb = new StringBuilder();
		while(in.hasNext()) {
		    sb.append(in.next());
		}
		in.close();
		
		
		
		Permanente perm = new Permanente(new Gson().fromJson(sb.toString(),Config.class));
		Thread RunningThread = new Thread(perm); 
		RunningThread.start();
		
		if(perm.config.plugins.discord_bot.token == null) {
			Scanner reader = new Scanner(System.in);
			System.out.println("Discord token is not present in config. Enter it here: "); //I dont want to store tokens in source for now.
			perm.config.plugins.discord_bot.token = reader.next();
			reader.close();
		}
		
		JDA jda = null;
		try {
			jda = new JDABuilder(AccountType.BOT).setToken(perm.config.plugins.discord_bot.token).addEventListener(new Bot(perm)).build();
			System.out.println("Finished Building JDA!");
		} catch (LoginException e) {
			System.err.println("Error login token invalid or failed to login.");
			e.printStackTrace();
		}
		jda.awaitReady();
		jda.getPresence().setGame(Game.playing("Backing up peoples videos"));
		//jda.getPresence().setGame(Game.playing("Not working yet | Stay tuned!"));
	}
	@Override
	public void onMessageReceived(MessageReceivedEvent event) {
		User author = event.getAuthor(); 
		Message message = event.getMessage();
		ParseIt(message);
	}
	private void ParseIt(Message in) {
		HashMap<String, Runnable> dictMap = new HashMap<String, Runnable>();
		Commands commands = new Commands(this.Permanente);
		
		
		//Register your command here :) command name (String), Command method link (Lambda)
		dictMap.put("help", () -> commands.c_helpmenu(in));
		dictMap.put("addvideo", () -> commands.c_addvideo(in));
		dictMap.put("videoinfo", () -> commands.c_videoinfo(in));
		dictMap.put("removevideo", () -> commands.c_removevideo(in));
		dictMap.put("botinfo", () -> commands.c_botinfo(in));
		dictMap.put("list", () -> commands.c_list(in));
		dictMap.put("myvideos", () -> commands.c_myvideos(in));
		dictMap.put("stats", () -> commands.c_stats(in));
		dictMap.put("ping", () -> commands.c_ping(in));
		dictMap.put("whitelist", () -> commands.c_whitelist(in));
		dictMap.put("queue", () -> commands.c_queue(in)); //TODO replace with multi command queue 
		dictMap.put("activesessions", () -> commands.c_activesessions(in));
		dictMap.put("p2p", () -> commands.c_p2p(in));
		dictMap.put("raw", () -> commands.c_raw(in));
		//dictMap.put("search", () -> commands.c_search(in)); //Not completed
		
		//deny any messages that dont contain our prefix.
		
		for(Object e : dictMap.keySet().toArray()) {
			if(e instanceof String) {
				if(in.getContentRaw().startsWith(Bot.prefix+(String)e)) {
					new Thread(dictMap.get(e)).start();
				}
			}
		}
	}
}
