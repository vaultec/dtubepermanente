package io.permanente.plugins;

import java.awt.Color;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;
import org.json.JSONTokener;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import io.permanente.primitives.Session;
import io.permanente.primitives.VideoEntry;
import io.ipfs.api.IPFS;
import io.ipfs.api.MerkleNode;
import io.ipfs.cid.Cid;
import io.ipfs.multihash.Multihash;
import io.permanente.Permanente;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;


/**
 * 
 * 
 * @author vaultec
 */
public class Commands {
	private Permanente Permanente;
	
	public Commands(Permanente perm) {
		this.Permanente = perm;
		Bot.prefix = this.Permanente.config.plugins.discord_bot.prefix;
	}
	public void c_helpmenu(Message in) {
		MessageChannel channel = null;
		
		if (in.isFromType(ChannelType.TEXT))       
        {
			channel = in.getTextChannel();
        } else if(in.isFromType(ChannelType.PRIVATE)) {
        	channel = in.getPrivateChannel();
        }
		EmbedBuilder help_menu_eb = new EmbedBuilder();
		
		help_menu_eb.setTitle("**DTube Permanente - Command list**", null);

		help_menu_eb.setColor(Color.blue);

		//help_menu_eb.setDescription(
		//		Bot.prefix + "addvideo - backups video with a specified URL\n"
		//		+"");
		
		
		//help_menu_eb.setImage("https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Monero-Logo.svg/2000px-Monero-Logo.svg.png");
		help_menu_eb.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/c/c2/IPFS_logo.png");
		help_menu_eb.setFooter("DTube Permanente bot ", null);
		help_menu_eb.addField(Bot.prefix + "addvideo <URL>", "backups video with a specified URL, only works for DTube videos",false);
		help_menu_eb.addField(Bot.prefix + "videoinfo <ID>", "retrieves IPFS hashes and metadata of specified video ID ",false);
		help_menu_eb.addField(Bot.prefix + "removevideo <ID>", "removes video with specified ID ",false);
		help_menu_eb.addField(Bot.prefix + "botinfo","Prints information about bot",false);
		help_menu_eb.addField(Bot.prefix + "list","List all videos in Database.video_database",false);
		help_menu_eb.addField(Bot.prefix + "myvideos","displays all videos you have added", false);
		help_menu_eb.addField(Bot.prefix + "stats", "get current stats. Storage usage, video count etc.", false);
		help_menu_eb.addField(Bot.prefix + "ping", "get bot response time", false);
		help_menu_eb.addField(Bot.prefix + "whitelist", "work in progress, designed for whitelisting users", false);
		help_menu_eb.addField(Bot.prefix + "queue", "queue control, information about queue.", false);
		
		
		//in.getAuthor().openPrivateChannel().queue((channel) ->
	    //{
	    //    channel.sendMessage(help_menu_eb.build()).queue();
	    //});
		channel.sendMessage(help_menu_eb.build()).queue();
	}
	public void c_addvideo(Message in) {
		MessageChannel channel = null;
		
		if (in.isFromType(ChannelType.TEXT))       
        {
			channel = in.getTextChannel();
        } else if(in.isFromType(ChannelType.PRIVATE)) {
        	channel = in.getPrivateChannel();
        }
		
		EmbedBuilder response_eb = new EmbedBuilder();
		response_eb.setColor(Color.blue);
		if(this.Permanente.config.plugins.discord_bot.whitelist_enable) {
			if(!this.Permanente.config.plugins.discord_bot.Whitelist.contains(in.getAuthor().getIdLong()) && !this.Permanente.config.plugins.discord_bot.ownerlist.contains(in.getAuthor().getIdLong())) {
				response_eb.setDescription("Sorry, you are not authorized!\n If you feel this is an error please contact the bot owner.");
				channel.sendMessage(response_eb.build()).queue();
				return;
			}
		}
		
		
		VideoEntry ve = this.Permanente.dtube.CreateEntry(in.getContentRaw().replace(Bot.prefix + "addvideo ", ""));
		ve.misc.discord_owner_ID = in.getAuthor().getIdLong();
		
		int status = this.Permanente.dtube.AddVideoToQueue(ve);
		if(status == -1) {
			response_eb.setDescription("[Error] Video already exists in Database.video_database");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		if(status == -2) {
			response_eb.setDescription("[Error] Video is already in download queue");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		if(status == -3) {
			response_eb.setDescription("[Error] Queue has reached maximum size try again later ");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		
		
		response_eb.setDescription("Video added to queue \n" + 
				"ID: " + ve.ID + " (mark this down for easy referencing)\n"
				);
		
	    channel.sendMessage(response_eb.build()).queue();
	    
	    for(int x = 0; x < 1000/* 1000 second timeout*/; x++) {
			if(this.Permanente.Database.video_database.containsKey(String.valueOf(ve.ID))) {
				channel.sendMessage(in.getAuthor().getAsMention() + " Video ID of " + ve.ID + " has completed backup to IPFS!").queue();
				break;
			}
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	    
	    
	}
	public void c_videoinfo(Message in) {
		MessageChannel channel = null;
		
		if (in.isFromType(ChannelType.TEXT))       
        {
			channel = in.getTextChannel();
        } else if(in.isFromType(ChannelType.PRIVATE)) {
        	channel = in.getPrivateChannel();
        }
		EmbedBuilder response_eb = new EmbedBuilder();
		response_eb.setColor(Color.blue);
		
		
		Long ID;
		try {
			ID = Long.parseLong(in.getContentRaw().replace(Bot.prefix + "videoinfo ", ""));
		} catch(NumberFormatException e) {
			response_eb.setDescription("Invalid ID");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		VideoEntry ev = this.Permanente.dtube.videoinfo(ID);
		if(ev == null) { 
			response_eb.setDescription("Video not found");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		
		try {
			//System.out.println(new MerkleNode(ev.vh_240p.toString()).toJSONString());
			//System.out.println(new String(this.Permanente.ipfs.dag.get(new Cid(ev.vh_240p))));
			JSONObject node = new JSONObject(new JSONTokener(new String(this.Permanente.ipfs.dag.get(new Cid(ev.vh_240p)))));
			
			//System.out.println(node);
			//System.out.println(new String(this.Permanente.ipfs.dag.get(new Cid(ev.vh_240p))));
			//System.out.println(new String(this.Permanente.Database.tobytes()));
			//System.out.println(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(this.Permanente.Database.permid_database));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String extraout = "";
		if(ev.vh_720p != null) {
			extraout+= "\n720p IPFS hash: " + ev.vh_720p.toBase58();
		}
		if(ev.vh_1080p != null) {
			extraout+= "\n1080p IPFS hash: " + ev.vh_1080p.toBase58();
		}
		
		response_eb.setDescription("ID: " + ev.ID +"\n" 
				+ "240p IPFS hash: " + ev.vh_240p.toBase58()
				+ "\n480p IPFS hash: " + ev.vh_480p.toBase58()
				+ extraout
				);
		
		channel.sendMessage(response_eb.build()).queue();
	}
	public void c_removevideo(Message in) {
		MessageChannel channel = null;
		
		EmbedBuilder response_eb = new EmbedBuilder();
		response_eb.setColor(Color.blue);
		
		if (in.isFromType(ChannelType.TEXT))       
        {
			channel = in.getTextChannel();
        } else if(in.isFromType(ChannelType.PRIVATE)) {
        	channel = in.getPrivateChannel();
        }
		
		
		
		Long ID;
		System.out.println("[DEBUG] Video with ID of " + in.getContentRaw().replace(Bot.prefix + "removevideo ", "") + " is trying to be removed");
		try {
			ID = Long.parseLong(in.getContentRaw().replace(Bot.prefix + "removevideo ", ""));
		} catch(NumberFormatException e) {
			response_eb.setDescription("Invalid ID");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		if(this.Permanente.config.plugins.discord_bot.whitelist_enable) {
			if(!this.Permanente.config.plugins.discord_bot.ownerlist.contains(in.getAuthor().getIdLong()) && !(this.Permanente.dtube.videoinfo(ID).misc.discord_owner_ID == in.getAuthor().getIdLong())) {
				response_eb.setDescription("Sorry, you are not authorized!\n If you feel this is an error please contact the bot owner.");
				channel.sendMessage(response_eb.build()).queue();
				return;
			}
		}
		
		
		int status = this.Permanente.dtube.RemoveVideo(ID);
		if(status == -1) {
			response_eb.setDescription("Error video not found");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		if(status == -2) {
			response_eb.setDescription("Unexpected error occurred, Check logs");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		response_eb.setDescription("Video successfully removed ID: " + ID);
		channel.sendMessage(response_eb.build()).queue();
	}
	public void c_botinfo(Message in) {
		MessageChannel channel = null;
		
		EmbedBuilder response_eb = new EmbedBuilder();
		response_eb.setColor(Color.blue);
		
		if (in.isFromType(ChannelType.TEXT))       
        {
			channel = in.getTextChannel();
        } else if(in.isFromType(ChannelType.PRIVATE)) {
        	channel = in.getPrivateChannel();
        }
		response_eb.appendDescription("See https://gitlab.com/vaultec/dtubepermanente for more information");
		response_eb.appendDescription("v0.2; Developer: Asuska | @vaultec");
		channel.sendMessage(response_eb.build()).queue();
	}
	public void c_list(Message in) {
		//TODO: Replace with permanente.list... Need a method for permanente that creates a list of all videos, Can be used later on in API
		
		
		MessageChannel channel = null;
		
		EmbedBuilder response_eb = new EmbedBuilder();
		response_eb.setColor(Color.blue);
		
		if (in.isFromType(ChannelType.TEXT))       
        {
			channel = in.getTextChannel();
        } else if(in.isFromType(ChannelType.PRIVATE)) {
        	channel = in.getPrivateChannel();
        }
		if(this.Permanente.Database.video_database.size() == 0) { 
			response_eb.setDescription("Database is empty");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		response_eb.setTitle("Total videos: " + this.Permanente.Database.video_database.keySet().size());
		//Creates field full of videoentry(s)  
		for(Object e : this.Permanente.Database.video_database.keySet()) {
			//System.out.println(this.Permanente.config.Database.video_database.get(e).ID);
			
			response_eb.addField(((VideoEntry) this.Permanente.Database.video_database.get(e)).ID +"", 
					"@"+((VideoEntry) this.Permanente.Database.video_database.get(e)).misc.author 
					+ "/" +((VideoEntry) this.Permanente.Database.video_database.get(e)).misc.permlink , true);
			this.Permanente.Database.video_database.get(e);
		}
		
		channel.sendMessage(response_eb.build()).queue();
	}
	public void c_myvideos(Message in) {
		MessageChannel channel = null;
		
		if (in.isFromType(ChannelType.TEXT))       
        {
			channel = in.getTextChannel();
        } else if(in.isFromType(ChannelType.PRIVATE)) {
        	channel = in.getPrivateChannel();
        }
		
		EmbedBuilder response_eb = new EmbedBuilder();
		response_eb.setColor(Color.blue);
		if(this.Permanente.Database.video_database.size() == 0) { 
			response_eb.setDescription("Database is empty");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		response_eb.addField("WARNING: ","below does not mean ANYTHING, these videos are ones you have ADDED, this does not mean you own them!",false);
		for(Object e : this.Permanente.Database.video_database.keySet()) {
			//System.out.println(this.Permanente.config.Database.video_database.get(e).ID);
			if(((VideoEntry) this.Permanente.Database.video_database.get(e)).misc.discord_owner_ID == in.getAuthor().getIdLong()) {
				response_eb.addField(((VideoEntry) this.Permanente.Database.video_database.get(e)).ID +"", 
						"@"+((VideoEntry) this.Permanente.Database.video_database.get(e)).misc.author 
						+ "/" +((VideoEntry) this.Permanente.Database.video_database.get(e)).misc.permlink , true);
			}
		}
		
		channel.sendMessage(response_eb.build()).queue();
	}
	public void c_stats(Message in) {
		//TODO: Cache the stats, less IO usage
		
		
		
		MessageChannel channel = null;
		
		if (in.isFromType(ChannelType.TEXT))       
        {
			channel = in.getTextChannel();
        } else if(in.isFromType(ChannelType.PRIVATE)) {
        	channel = in.getPrivateChannel();
        }
		
		EmbedBuilder response_eb = new EmbedBuilder();
		response_eb.setColor(Color.pink);
		
		/*if(this.Permanente.Database.video_database.size() == 0) { 
			response_eb.setDescription("Database is empty");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}*/
		
		long size = 0;
		for(Object e : this.Permanente.Database.video_database.keySet()) {
			/*
			 * Deprecated
			size = size + this.Permanente.ipfs.cat(this.Permanente.Database.video_database.get(e).vh_240p).length;
			size = size + this.Permanente.ipfs.cat(this.Permanente.Database.video_database.get(e).vh_480p).length;
			if(this.Permanente.Database.video_database.get(e).vh_720p != null) 
				size = size + this.Permanente.ipfs.cat(this.Permanente.Database.video_database.get(e).vh_720p).length;
			if(this.Permanente.Database.video_database.get(e).vh_1080p != null) 
				size = size + this.Permanente.ipfs.cat(this.Permanente.Database.video_database.get(e).vh_1080p).length; */
			size = size + ((VideoEntry) this.Permanente.Database.video_database.get(e)).misc.size;
		}
		
		Map<?, ?> stats;
		try {
			stats = this.Permanente.ipfs.stats.bw();
		} catch (IOException e1) {
			e1.printStackTrace();
			response_eb.setDescription("An error has occurred check logs");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		try {
			response_eb.setTitle("Permanente node stats");
			response_eb.setDescription(
					"IPFS storage usage: " + size/(1000*1000)/*1MMB*/ + " MB \n"
					+ "Videos: " + this.Permanente.Database.video_database.keySet().size() + "\n"
					+ "Bandwidth In: " + ((Double) stats.get("RateIn")).intValue()/1000 + "KB/s\n"
					+ "Bandwidth Out: " + ((Double) stats.get("RateOut")).intValue()/1000 + "KB/s\n"
					+ "No. of peers: " + this.Permanente.ipfs.swarm.peers().size()
					);
		} catch(IOException ex) {
			response_eb.setDescription("Oops looks an error has occured! well this is embarressing!\n Check log file for details.");
			ex.printStackTrace();
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		
		channel.sendMessage(response_eb.build()).queue();
	}
	public void c_search(Message in) {
		//Not completed
		//TODO: Make the ability to get a list of potential IDs for a video based off search term. Look through permlink, JSON meta, descript, etc
		MessageChannel channel = null;
		
		if (in.isFromType(ChannelType.TEXT))       
        {
			channel = in.getTextChannel();
        } else if(in.isFromType(ChannelType.PRIVATE)) {
        	channel = in.getPrivateChannel();
        }
		
		EmbedBuilder response_eb = new EmbedBuilder();
		response_eb.setColor(Color.blue);
	} 
	public void c_ping(Message in) {
		MessageChannel channel = null;
		
		if (in.isFromType(ChannelType.TEXT))       
        {
			channel = in.getTextChannel();
        } else if(in.isFromType(ChannelType.PRIVATE)) {
        	channel = in.getPrivateChannel();
        }
		in.getCreationTime().toEpochSecond();
		Instant.now().getEpochSecond();
		
		EmbedBuilder response_eb = new EmbedBuilder();
		response_eb.setColor(Color.blue);
		
		response_eb.setDescription("Pong! " + (Instant.now().toEpochMilli()-in.getCreationTime().toInstant().toEpochMilli())/10 + "ms ");
		channel.sendMessage(response_eb.build()).queue();
	}
	public void c_whitelist(Message in) {
		MessageChannel channel = null;
		
		if (in.isFromType(ChannelType.TEXT))       
        {
			channel = in.getTextChannel();
        } else if(in.isFromType(ChannelType.PRIVATE)) {
        	channel = in.getPrivateChannel();
        }
		
		EmbedBuilder response_eb = new EmbedBuilder();
		response_eb.setColor(Color.blue);
		
		String[] args = in.getContentRaw().split(" ");
		
		//Checks if user is in owner list
		if(!this.Permanente.config.plugins.discord_bot.ownerlist.contains(in.getAuthor().getIdLong())) {
			response_eb.setDescription("Sorry, you are not authorized!\n If you feel this is an error please contact the bot owner.");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		
		if(args.length <= 1) {
			response_eb.setDescription("Invalid argument, Type `"+Bot.prefix+"whitelist help` for info");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		try {
			switch(args[1]) {
			case "help":
				response_eb.setTitle(Bot.prefix +"whitelist <arguments>");
				response_eb.addField("ls","list whitelist",false);
				response_eb.addField("add","add user to whitelist (next argument must contain @user)",false);
				response_eb.addField("remove","remove user from whitelist (next argument must contain @user)",false);
				channel.sendMessage(response_eb.build()).queue();
				return;
			case "ls":
				if(this.Permanente.config.plugins.discord_bot.Whitelist.size() == 0) { 
					response_eb.setDescription("Whitelist is empty");
					channel.sendMessage(response_eb.build()).queue();
					return;
				}
				for(long e : this.Permanente.config.plugins.discord_bot.Whitelist) {
					response_eb.addField(in.getJDA().getUserById(e).toString(),"", false);
				}
			case "add":
				if(in.getMentionedUsers().get(0) == null ) {
					response_eb.setDescription("Invalid arguments. User name is required ");
					channel.sendMessage(response_eb.build()).queue();
					return;
				}
				if(!this.Permanente.config.plugins.discord_bot.Whitelist.contains(in.getMentionedUsers().get(0).getIdLong())) {
					this.Permanente.config.plugins.discord_bot.Whitelist.add(in.getMentionedUsers().get(0).getIdLong());
					response_eb.setDescription("sucessfully added user: " + in.getMentionedUsers().get(0).getName());
					this.Permanente.config.Save();
				} else {
					response_eb.setDescription("User already whitelisted.");
				}
				channel.sendMessage(response_eb.build()).queue();
				return;
			case "remove":
				if(in.getMentionedUsers().get(0) == null ) {
					response_eb.setDescription("Invalid arguments. User name is required ");
					channel.sendMessage(response_eb.build()).queue();
					return;
				}
				if(this.Permanente.config.plugins.discord_bot.Whitelist.contains(in.getMentionedUsers().get(0).getIdLong())) {
					this.Permanente.config.plugins.discord_bot.Whitelist.remove(in.getMentionedUsers().get(0).getIdLong());
					response_eb.setDescription("sucessfuly remove user: " + in.getMentionedUsers().get(0).getName());
					this.Permanente.config.Save();
				} else {
					response_eb.setDescription("User does not exist.");
				}
				channel.sendMessage(response_eb.build()).queue();
				return;
			}
		} catch(Exception ex) {
			response_eb.setDescription("Oops looks an error has occured! well this is embarressing!\n Check log file for details.");
			ex.printStackTrace();
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		
		System.out.print(args[0]);
		
		//in.getMentionedMembers();
		
		
	}
	public void c_queue(Message in) {
		MessageChannel channel = null;
		
		if (in.isFromType(ChannelType.TEXT))       
        {
			channel = in.getTextChannel();
        } else if(in.isFromType(ChannelType.PRIVATE)) {
        	channel = in.getPrivateChannel();
        }
		
		EmbedBuilder response_eb = new EmbedBuilder();
		response_eb.setColor(Color.blue);
		String[] args = in.getContentRaw().split(" ");
		
		//Checks if user is in owner list
		if(this.Permanente.config.plugins.discord_bot.whitelist_enable) {
			if(!this.Permanente.config.plugins.discord_bot.ownerlist.contains(in.getAuthor().getIdLong())) {
				response_eb.setDescription("Sorry, you are not authorized!\n If you feel this is an error please contact the bot owner.");
				channel.sendMessage(response_eb.build()).queue();
				return;
			}
		}
		
				
		if(args.length <= 1) {
			response_eb.setDescription("Invalid argument, Type `"+Bot.prefix+"queue help` for info");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		try {
			switch(args[1]) {
			case "help":
				response_eb.setTitle("queue <arguments>");
				response_eb.addField("info","get current size, max size.",false);
				response_eb.addField("clean","removes all entries from queue",false);
				channel.sendMessage(response_eb.build()).queue();
				return;
			case "info":
				response_eb.setTitle("Queue info");
				response_eb.setDescription("Current queue size: " + this.Permanente.List_Queue.size() 
				+ " Max queue size: " + this.Permanente.config.Max_Queue_Size
				);
				channel.sendMessage(response_eb.build()).queue();
				return;
			case "clean": 
				response_eb.setTitle("Queue clean");
				if(this.Permanente.List_Queue.size() == 0) {
					response_eb.setDescription("Nothing to remove, queue empty!");
					channel.sendMessage(response_eb.build()).queue();
					return;
				}
				String out = "";
				Iterator<VideoEntry> it = this.Permanente.List_Queue.iterator();
				while(it.hasNext()) {
					VideoEntry e = it.next();
					out+= "Removed " + e.ID + "\n";
					this.Permanente.List_Queue.remove(e);
				}
				response_eb.setDescription(out);
				channel.sendMessage(response_eb.build()).queue();
				return;
			}
		} catch(Exception ex) {
			response_eb.setDescription("Oops looks an error has occured! well this is embarressing!\n Check log file for details.");
			ex.printStackTrace();
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		
	}
	public void c_authuser(Message in) {
		//TODO: Authorize userid for headmaster and create authentication token.
		//TODO: Need to adjust for financial system (Aka blockchain payments)
	}
	public void c_activesessions(Message in) {
		MessageChannel channel = null;
		
		if (in.isFromType(ChannelType.TEXT))       
        {
			channel = in.getTextChannel();
        } else if(in.isFromType(ChannelType.PRIVATE)) {
        	channel = in.getPrivateChannel();
        }
		
		EmbedBuilder response_eb = new EmbedBuilder();
		response_eb.setColor(Color.blue);
		
		StringBuilder sb = new StringBuilder();
		Set<String> tempkeyset = this.Permanente.HTTPsessions.keySet();
		
		for(String e : tempkeyset) {
			//System.out.println(e);
			Session session = this.Permanente.HTTPsessions.get(e);
			//System.out.print(System.currentTimeMillis());
			//System.out.println(session.Lastping);
			
			
			String exper = null;
			long timetoexpire = ((session.timestamp+session.expirelength)-System.currentTimeMillis())/1000;
			if(timetoexpire > 0) {
				exper = timetoexpire+"s remaining";
			} else {
				exper = "expired";
			}
			sb.append(session.userid +" | " + session.cookie + " | " +  (System.currentTimeMillis()-session.lastseen)/1000 + "s ago | "+exper+"\n");
		}
		response_eb.setTitle("UserID | cookie | Last ping time | time left on lease");
		response_eb.setDescription(sb.toString());
		channel.sendMessage(response_eb.build()).queue();
	}
	public void c_p2p(Message in) {
		MessageChannel channel = null;
		
		if (in.isFromType(ChannelType.TEXT))       
        {
			channel = in.getTextChannel();
        } else if(in.isFromType(ChannelType.PRIVATE)) {
        	channel = in.getPrivateChannel();
        }
		
		EmbedBuilder response_eb = new EmbedBuilder();
		response_eb.setColor(Color.blue);
		
		String[] args = in.getContentRaw().split(" ");
		if(args.length <= 1) {
			response_eb.setDescription("Invalid argument, Type ```"+Bot.prefix+"p2p help``` for info");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		/*
		try {
			switch(args[1]) {
			case "help":
				response_eb.setTitle("!p2p <arguments>");
				response_eb.addField("peers","list active peers",false);
				channel.sendMessage(response_eb.build()).queue();
				return;
				
			case "peers":
				if(this.Permanente.p2p().peers().all().isEmpty()) {
					response_eb.setDescription("No connected peers.");
					channel.sendMessage(response_eb.build()).queue();
					return;
				}
				for(PeerAddress e : this.Permanente.p2p().peers().all()) {
					response_eb.addField(e.peerId().toString(),Long.toString(this.Permanente.p2p().peers().getPeerStatistic(e).getMeanRTT()), false);
					Buffer buffer = null;
					
					
					this.Permanente.p2p().dht.peer().sendDirect(e).object("Hello").start().await();
					this.Permanente.p2p().dht.peer().sendDirect(this.Permanente.p2p().dht.peerAddress()).start().await();
					
					
					buffer = new Buffer(Unpooled.wrappedBuffer("Hello world!".getBytes()));
					
					
					FutureSend futureSend = this.Permanente.p2p().dht.send(e.peerId()).buffer(buffer).start();
					futureSend.await();
				}
				System.out.println(this.Permanente.p2p().peers());
				channel.sendMessage(response_eb.build()).queue();
				return;
			case "test":
				//this.Permanente.p2p().dht.peer.sendDirect(recipientAddress)
				return;
			}
		} catch(Exception ex) {
			response_eb.setDescription("Oops looks an error has occured! well this is embarressing!\n Check log file for details.");
			ex.printStackTrace();
			channel.sendMessage(response_eb.build()).queue();
			return;
		}*/
	}
	public void c_raw(Message in) {
		MessageChannel channel = null;
		
		if (in.isFromType(ChannelType.TEXT))       
        {
			channel = in.getTextChannel();
        } else if(in.isFromType(ChannelType.PRIVATE)) {
        	channel = in.getPrivateChannel();
        }
		
		EmbedBuilder response_eb = new EmbedBuilder();
		response_eb.setColor(Color.blue);
		
		String[] args = in.getContentRaw().split(" ");
		
		if(!this.Permanente.config.plugins.discord_bot.Whitelist.contains(in.getAuthor().getIdLong())) {
			response_eb.setDescription("Sorry, you are not authorized!\n If you feel this is an error please contact the bot owner.");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		
		if(args.length <= 1) {
			response_eb.setDescription("Invalid argument, Type ```"+Bot.prefix+"raw help``` for info");
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
		Multihash mh = null;
		try {
			switch(args[1]) {
			case "help":
				response_eb.setTitle("!raw <arguments>");
				response_eb.addField("ls","list pins",false);
				response_eb.addField("add","pins hash to IPFS.",false);
				response_eb.addField("rm","unpins hash from IPFS.",false);
				channel.sendMessage(response_eb.build()).queue();
				return;
			case "ls":
				String list_txt  = "";
				for(Object e : this.Permanente.Database.raw_pins) {
					if(e instanceof Multihash) {
						list_txt += ((Multihash) e).toBase58() + "\n";
					}
				}
				if(this.Permanente.Database.raw_pins.isEmpty()) {
					list_txt = "Raw pins empty!";
				}
				response_eb.setDescription("``" + list_txt + "``");
				channel.sendMessage(response_eb.build()).queue();
				return;
			case "add":
				try {
					mh = Multihash.fromBase58(args[2]);
				} catch(Exception ex) {
					response_eb.setDescription("`Invalid IPFS hash.`");
					channel.sendMessage(response_eb.build()).queue();
					return;
				}
				if(!this.Permanente.Database.raw_pins.contains(mh)) {
					try {
						this.Permanente.ipfs.pin.add(mh);
						this.Permanente.Database.raw_pins.add(mh);
						//this.Permanente.Database.raw_pins.
					} catch(Exception ex) {
						ex.printStackTrace();
						response_eb.setDescription("`Failed to pin hash`");
						channel.sendMessage(response_eb.build()).queue();
						return;
					}
				} else {
					response_eb.setDescription("`Hash already added!`");
					channel.sendMessage(response_eb.build()).queue();
					return;
				}
				break;
			case "rm":
				try {
					mh = Multihash.fromBase58(args[2]);
				} catch(Exception ex) {
					response_eb.setDescription("`Invalid IPFS hash.`");
					channel.sendMessage(response_eb.build()).queue();
					return;
				}
				
				if(this.Permanente.Database.raw_pins.contains(mh)) {
					try {
						this.Permanente.ipfs.pin.rm(mh);
						this.Permanente.Database.raw_pins.remove(mh);
						response_eb.setDescription("`Sucessfully removed!`");
						channel.sendMessage(response_eb.build()).queue();
						return;
					} catch(Exception ex) {
						response_eb.setDescription("`Failed to unpin hash`");
						channel.sendMessage(response_eb.build()).queue();
						return;
					}
				} else {
					response_eb.setDescription("`Hash not pinned`");
					channel.sendMessage(response_eb.build()).queue();
					return;
				}
			}
		} catch(Exception ex) {
			response_eb.setDescription("Oops looks an error has occured! well this is embarressing!\n Check log file for details.");
			ex.printStackTrace();
			channel.sendMessage(response_eb.build()).queue();
			return;
		}
			
	}
}
