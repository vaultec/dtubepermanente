package io.permanente.primitives;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.mapdb.DataInput2;
import org.mapdb.DataOutput2;
import org.mapdb.Serializer;

import com.google.gson.Gson;

import io.ipfs.multihash.Multihash;

public interface Serializers {
	public class VideoEntrySerializer implements Serializer<VideoEntry>, Serializable {
		private static final long serialVersionUID = 4041283728452342593L;

		public void serialize(DataOutput2 out, VideoEntry value) throws IOException {
	    	/*ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        ObjectOutputStream oos = new ObjectOutputStream( baos );
	        oos.writeObject( value );
	        oos.close();
	        //out.writeBytes(baos.toString());*/
	        out.writeBytes(new Gson().toJson(value, VideoEntry.class));
	    }
	    public VideoEntry deserialize(DataInput2 in, int available) throws IOException {
	    	//System.out.println(in.readLine());
	    	//in.readFully(data);
	    	//byte[] data = new byte[in.readLine().getBytes().length];
	    	//data = in.readLine().getBytes();
	    	
	    	//System.out.println(new String(data));
	    	//System.out.println(new String(in.internalByteArray()));
	        /*ObjectInputStream ois = new ObjectInputStream( 
	                                        new ByteArrayInputStream(  data ) );
	        Object o = null;
			try {
				o = ois.readObject();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        ois.close();*/
	        return new Gson().fromJson(in.readLine(), VideoEntry.class);
	    }
	}
	public class MultihashSerializer implements Serializer<Multihash>, Serializable {
		private static final long serialVersionUID = 7876061341203527796L;
		
		public void serialize(DataOutput2 out, Multihash value) throws IOException {
	    	/*ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        ObjectOutputStream oos = new ObjectOutputStream( baos );
	        oos.writeObject( value );
	        oos.close();
	        out.writeBytes(baos.toString());*/
	        out.writeBytes(new Gson().toJson(value, Multihash.class));
	    }
	    public Multihash deserialize(DataInput2 in, int available) throws IOException {
	    	/*byte [] data = null;
	    	in.readFully(data);
	    	System.out.println(new String(data));
	    	System.out.println(new String(in.internalByteArray()));
	        ObjectInputStream ois = new ObjectInputStream( 
	                                        new ByteArrayInputStream(  data ) );
	        Object o = null;
			try {
				o = ois.readObject();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        ois.close();*/
	        return new Gson().fromJson(new String(in.readLine()), Multihash.class);
	    }
	}
}
