package io.permanente.primitives;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Random;

import org.json.JSONObject;
import org.mapdb.DataInput2;
import org.mapdb.DataOutput2;
import org.mapdb.Serializer;

import com.google.gson.Gson;

import io.ipfs.multihash.Multihash;
import io.permanente.primitives.IPFSVideo;

/*
 * This defines what a video *should* look like in the database, removing need to complex jsonobjects defining videos
 * Video hashes are defined as IPFS Multihash
 * Goals... Entire video should be representable here, easy
 */

/**
 * 
 * @author vaultec
 * @since v1.0
 */
public class VideoEntry implements Serializable {
	private static final long serialVersionUID = -5687807377963156052L;
	
	//public entryType type;
	
	public enum entryType implements Serializable {
		Legacy, New
	}
	
	public misc misc = new misc();
	
	public long ID; //ID should be random when calling VideoEntry() constructor (Can i be changed if needed)
	public long expire; //Timestamp of expiration
	
	public Multihash vh_src; //Only used for uploader (most video source files are 200Mb+, after resizing and compression ~20Mb)
	public Multihash vh_240p;
	public Multihash vh_480p;
	public Multihash vh_720p;
	public Multihash vh_1080p;
	public Multihash vh_snap;
	
	//Updated system
	public transient IPFSVideo video;
	
	//This is meant for uploader but can be used other ways, (User creates post metadata is stored, potentially can be scheduled to post at certain time)
	public transient JSONObject video_metadata; //Optional (I want metadata to be stored locally eliminating calling API for everything) essentially all the data from steemit.
	
	//Extra information goes here, not sure what this could be practically used for.
	public class misc implements Serializable {
		private static final long serialVersionUID = -6353519969438001640L;
		public String URL;
		public String author;
		public String permlink;
		public long discord_owner_ID;
		
		public long la_240p; 
		public long la_480p;
		public long la_720p;
		public long la_1080p;
		
		//For later use
		public long v240p_size;
		public long v480p_size;
		public long v720p_size;
		public long v1080p_size;

		public long size;
	}
	
	//TODO: add a set of constructors and methods for simply converting this object into other forms/Doing tasks. remove this comment once done...
	//Example constructors; Initialize data from d.tube or steemit URL, Initilize from JSONObject/Solo metadata, etc...
	//Examples for methods; Export data, read data, Download video (probably not needed), convert to JSON.
	public VideoEntry() {
		//Create new video 
		this.ID = new Random().nextInt( (int) Math.pow(10, 10));
	}
	
	public VideoEntry(JSONObject json) {
		//if(json.has("vh_240p"))
			//this.vh_240p = new Multihash(json.getString("vh_240p"));
		
		//if(json.has("vh_480p"))
	}
	
	public VideoEntry(long ID) {
		//Idea to have video be created with certain ID. ID can be based off URL/permlink instead of random value.
	}
	
	public Multihash video_hash(String resolution) {
		if(video !=null) {
			return video.video_index.get(resolution);
		}
		if(resolution.equals("240p")) {
			return vh_240p;
		} else if(resolution.equals("480p")) {
			return vh_480p;
		} else if(resolution.equals("720p")) {
			return vh_720p;
		} else if(resolution.equals("1080p")) {
			return vh_1080p;
		} else {
			return null;
		}
	}
	public void video_hash(String resolution, Multihash hash) {
		if(video != null) {
			video.video_index.put(resolution, hash);
		}
		if(resolution.equals("240p")) {
			vh_240p = hash;
		} else if(resolution.equals("480p")) {
			vh_480p = hash;
		} else if(resolution.equals("720p")) {
			vh_720p = hash;
		} else if(resolution.equals("1080p")) {
			vh_1080p = hash;
		}
	}
	public Multihash snap() {
		if(this.video.snap != null) {
			return this.video.snap;
		}
		return null;
	}
	public String ToJSON() {
		return new Gson().toJson(this, VideoEntry.class);
	}
	public static VideoEntry fromJSON(String json) {
		return new Gson().fromJson(json, VideoEntry.class);
	}
	
	
	public boolean equals(VideoEntry e) {
		if(e.ID == this.ID) {
			return true;
		}
		return false;
	}
}
