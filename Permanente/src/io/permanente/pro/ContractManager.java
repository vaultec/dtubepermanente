package io.permanente.pro;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import io.permanente.Config;
import io.permanente.Permanente;

/**
 * Contract management class 
 * DOCUMENTATION
 * Contract manager controls all aspects of storage contracts. Length, ID, usage, expiration date, buffer time etc.
 * This should be completely independent of a subscription manager/payment processor. (used in discord bots, online registration)
 * 
 * TODO: Create contract class.
 * TODO: Payment processor. Originally was going to be payment processor.
 * 
 * @author vaultec
 * @since v0.3
 * @implement v0.5
 */
public class ContractManager extends Thread {
	private Permanente permanente;
	private Config config;
	//String -> Transaction memo ID;
	private HashMap<String, PendingTrans> PendingTransactions = new HashMap<String, PendingTrans>();
	
	long clockdelay = 15000; //Time in ms to delay clock check.
	
	public ContractManager(Permanente permanente, Config config) {
		this.permanente = permanente; 
		this.config = config;
	}
	
	public void run() {
		
		for( ; ; ) {
			
			
			
			
			
			try {
				TimeUnit.MILLISECONDS.sleep(this.clockdelay);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	class PendingTrans {
		
	}
}
