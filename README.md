# - Permanente - IPFS dynamic storage network



#### Overview
* Easy to use as library
* Crossplatform (Dependant on IPFS, however remote node can be used)
* Relatively easy to compile
* JSON-RPC as API (Planned, Under Contruction)
* Adding/removing videos with ease through API/webinterface from IPFS node.
* Multithreaded downloading (Useful for large servers)
* Expandable 
* Fast java not nodejs 
* Cluster networks (An idea of a mining pool like network of IPFS nodes connected to a headmaster) (Planned, API needed first)
* GatewayPushing, Pre-caches the video data on multi gateways (tested, Works!). I noticed after awhile that videos that were just uploaded are slow.

# Installation/Build
### Linux/MacOSX
1. Download ```git clone https://gitlab.com/vaultec/dtubepermanente.git```
2. Change that directory ```cd dtubepermanente```
3. (Optional) Change branch to v0.2b ```git checkout v0.2b```
4. Install dependancies and compile ```chmod +x quickbuild.sh; ./quickbuild.sh``` <br>
This will ask for your password to install maven and java-jdk. If you already have these installed press ctrl + c to escape <br>
It will output permanente.jar into current working directory

quick entire command: ```git clone https://gitlab.com/vaultec/dtubepermanente.git; cd dtubepermanente; chmod+x quickbuild.sh; ./quickbuild.sh```

### Windows
TODO
#### Usage
Server/Bot:
```
usage: Permanente v0.2
 -a,--api                 When flag is set, API server is started
 -ap,--apiport <arg>      Set API port, 0-65563
 -bot,--bot               Starts discord bot. TODO
 -bt,--bot.token <arg>    Specify discord bot token. (defaults to config
                          without) TODO
 -cf,--configfile <arg>   Specify config file location.
 -d,--default             Use default configuration location.
                          (%Workingdirectory%/Config.json) RECOMMENDED
 -fh,--ipfshost <arg>     Specify ipfs host, must be in ipfs host format.
                          (defaults to localhost:5001)
 -h,--headmaster          Operate a headmaster on API. (Enables API)
 -s,--saveconfig          Saves configuration that is specified.
 -st,--exitonsave         Exits program after data save.
```
Client:
```
usage: Permanente Client v0.2
 -cf,--configfile <arg>   Specify config file location.
 -d,--default             Use default configuration location.
                          (%Workingdirectory%/ClientConfig.json)
                          RECOMMENDED
 -h,--headmaster <arg>    Specify headmaster URL. (Overrides config)
 -s,--saveconfig          Saves configuration that is specified.
 -st,--exitonsave         Exits program after data save.
 -t,--token <arg>         Specify authentication token.(Overrides config)
 -u,--userid <arg>        Specify userid. (Overrides config)
```
Other settings are not built into command line arguments, but fully changeable via config.json and clientconfig.json

See [config.java](https://gitlab.com/vaultec/dtubepermanente/blob/dev-01/DtubePermanente/src/io/permanente/Config.java) and [clientconfig.java](https://gitlab.com/vaultec/dtubepermanente/blob/dev-01/DtubePermanente/src/io/permanente/client/ClientConfig.java)


# NOTE/MISC
Join our (I mean my) discord https://discord.gg/nEfNgVP



### OFFICIAL release signing key
For the advanced users who want to verify releases.
All binary releases should be signed.

IPFS hash: QmVhPvcQ3rdMKRxjonjqcNDDaYuwHJYEh4HpSrR6yLe77p <br>
Fingerprint: 75D07488ACA53CD0ED96D429CA9811860B0B9520 <br>
[IPFS link](https://ipfs.io/ipfs/QmVhPvcQ3rdMKRxjonjqcNDDaYuwHJYEh4HpSrR6yLe77p)

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEXBm98BYJKwYBBAHaRw8BAQdAwssGVUdGVfSla4ellhUY3TL6T9tkY3ZjXOoE
FbaPGUa0F1ZhdWx0ZWMgUmVsZWFzZSBTaWduaW5niJYEExYIAD4WIQR10HSIrKU8
0O2W1CnKmBGGCwuVIAUCXBm98AIbAwUJA8NMUAULCQgHAgYVCgkICwIEFgIDAQIe
AQIXgAAKCRDKmBGGCwuVIDdgAQDSnjUgNNZnwuEtFWAmfYkpx/GaDRno/ECkMubX
qyhVbwD/R3bMWgORUNz9xckghPGn/FlLs3iNNKW8DLOWuenVRwK4OARcGb3wEgor
BgEEAZdVAQUBAQdACKMhnR3tJDx4bKK8OdUmGjzWbKzKSq/iQxDBOCj2RTYDAQgH
iH4EGBYIACYWIQR10HSIrKU80O2W1CnKmBGGCwuVIAUCXBm98AIbDAUJA8NMUAAK
CRDKmBGGCwuVIH9TAQDwLkPc6/9eV9GjIoqWS32Y6Z5ExA5p3leRD4f3p0dcogD6
AwXOU17R4KavhRtORRzvckZ/YTWGVPkhevaOPh9MVgo=
=n6IJ
-----END PGP PUBLIC KEY BLOCK-----
```