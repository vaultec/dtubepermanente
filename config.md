# Config.json 
Current as of `v0.3` for permanente full instance.

### general
`"API_enabled": true`
Enables an API server (no documentation yet!). If headmaster is enabled API will be enabled too! (minor issues between headmaster and API)

`"gc": false`
Deletes VideoEntry(s) with expired date. Removes data from IPFS.

`"Worker_maxthreads": 2`
Maximum number of threads to download/pin simultaneously.

` "API_Port": 8090`
Port for API to listen on. Binds on all interfaces.

`"Max_Queue_Size": 16,`
Maximum queue size. Queue will fill up if all threads are busy. 

`"IPFS_HOST": "/ip4/127.0.0.1/tcp/5001"`
IPFS API location in multiaddr format. Default to localhost.
multiaddr example: `/ip4/remote ipfs API/tcp/port`

#### plugins
`"plugins": {"enabled":[]}`
Specifies which plugin you want to enable, after you have enabled a plugin. You must start permanente in order to generate the config for a particular plugin.
example: `"plugins": {"enabled":["discord_bot"]}`
available plugins: `discord_bot, headmaster, gatewaypusher`
#### plugins.headmaster
`"polling_delay": 30000`
Specifies the time between polling operations in milliseconds (ms).

`"sat": 10,`
Specifies the amount of nodes per object in dynamic allocation. This will maintain a minimum of 10 nodes storing a single objects. (Redundancy) 

`"motd": "Welcome to DTube network cluster network! Join our discord to keep updated with latest news!"`
The welcome message for client connecting to headmaster.


#### plugins.discord_bot 
`"whitelist_enable": false`
Denies access to add or remove for anyone not on whitelist or ownerlist.

`"Whitelist": []`
You can manually add a discord account by supplying the account ID.

`"ownerlist": [],`
Same as whitelist. You must have your discord ID inserted in order to allow access whitelisting other users or using it yourself. (if whitelist is enabled)
example: `"ownerlist": [264266989973929985]`

`"token": "discord bot token here"`
Supply discord bot token. required for discord bot to function

`"prefix": "!"`
The command prefix. If prefix is `?`, instead of `!addvideo` it will be `?addvideo`. Useful for keeping multiple bots separate. 

