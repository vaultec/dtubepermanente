Older versions; TODO

#### v0.3
* Full logging support
* Cleanup
* Better API
* Build codebase for future versions
* Build stronger DAPI base

#### v0.4
* Migrate database to using improved format.
* Begin migration to improved dtube format. Video ID replace with QmHash
* Cluster network IPFS cleanup, clients remove videos not in headmaster's index, stronger DAPI protocol. 
* Start on P2P codebase.
* Dynamic allocation setup
* Cluster network identifier is IPFS Object, client is identifiable with QmHash.

### v0.5
* Full P2P beta for cluster networks. Syncable market place.
* Cluster network contracts.
* Full scale network.
* Blockchain based contract market?