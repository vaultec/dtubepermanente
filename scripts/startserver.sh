#!/bin/bash

if [ "$(command -v java)" = "" ]; then
	echo
	echo Error: Java not found
	exit 1
fi

java -cp ../permanente.jar io.permanente.Permanente $1 $2 $3 $5 $6 $7 $8 $9